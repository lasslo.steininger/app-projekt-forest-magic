package forestmagicpp.game;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import forestmagicpp.preset.ArgumentParser;
import forestmagicpp.preset.GameConfiguration;
import forestmagicpp.preset.PathType;
import forestmagicpp.preset.Position;
import forestmagicpp.preset.customgraphics.Path;

/**
 * Interpretiert aus der ersten Zeile einer Spielkonfiguration 
 * die einzelnen Waldfelder und ihre Verbindungen.
 * 
 * @author Daniel Wiedenmeier
 */
public class BoardGenerator {
    /** Spielkonfiguration */
    private GameConfiguration gameConfig;

    /** Zugriff auf die Kommandozeilenargumente */
    private ArgumentParser parser;

    /** Spielbrett */
    private GameBoard gameBoard;

    /**
     * Erzeugt einen BoardGenerator mit einem Agrumentparser 
     * 
     * @param argParser ArgumentParser für die Spielekonfiguration und Anzahl der eingegebenen Spieler
     * @throws Exception
     */    
    public BoardGenerator (ArgumentParser argParser) throws Exception {
        this.parser = argParser;
        this.gameConfig = new GameConfiguration(parser.getGameConfiguration());
        
        // Überprüft die übergebene Spielkonfiguration auf Gültigkeit
        checkConfig();  
        // Erstellt ein Spielbrett mit der gültigen Spielkonfiguration
        gameBoard = new GameBoard(gameConfig); 
    }

    /**
     * Überprüft die Konfiguration auf Korrektheit
     * 
     * @throws IllegalArgumentException wenn die Spielekonfiguration ungültig ist also wenn, Runden und Köder nicht im gültigen Wertebereich sind, Startfelder mehrfach besetzt werden, die Anzahl an Startfeldern nicht der der Spieler entspricht, Offene Runden nicht im gültigen Wertebereich sind, oder die Anzahl an Spielernamen nicht der Anzahl an Spielertypen entspricht                 
     */    
    private void checkConfig() throws IllegalArgumentException{
        List <Position> startPostions = gameConfig.getStartPositions();
        int count = 0;
        Iterator<Integer> iterator=gameConfig.getVisibleRounds().iterator();
        int temp;
        int maxRounds = gameConfig.getMaxNumberOfRounds();

        // Die Rundenanzahl darf nicht negativ sein
        if (maxRounds<=0){ 
            throw new IllegalArgumentException("Die Rundenanzahl darf nicht negativ oder null sein! [Es werden " + maxRounds+ " Runden gespielt]");
        }

        // Die Rundenanzahl darf nicht den Wert Integer.MAX_VALUE überschreiten
        if (maxRounds>Integer.MAX_VALUE){ 
            throw new IllegalArgumentException("Die Rundenanzahl ist zu hoch! [Es werden " + maxRounds+ " Runden gespielt]");
        }

        // Jeder Spielertyp muss einen Spielernamen haben
        if (parser.getPlayerNames().size()!=parser.getPlayerTypes().size()){ 
            throw new IllegalArgumentException("Anzahl der Spielernamen und Spielertypen ist nicht gleich! [Prüfen Sie playernames und playertypes nach]");            
        }

        // Offene Runden dürfen nicht negativ oder null sein und dürfen nicht nach der letzten Runde stattfinden
        while(iterator.hasNext()){ 
            temp = iterator.next();
            if (temp<=0){
                throw new IllegalArgumentException("Offene Runden dürfen nicht negativ oder null sein! [Fehler bei offener Runde " + temp +"]");
            }
            if (temp>maxRounds){
                throw new IllegalArgumentException("Offene Runden dürfen nicht größer als Rundenanzahl sein ! [Fehler bei offener Runde " + temp +"]");
            }
        }
    
        // Die angegebene Anzahl an Ködern darf nicht negativ sein
        if (gameConfig.getNumberOfCatRideUsages()<0){ 
            throw new IllegalArgumentException("Anzahl der Katzenköder darf nicht negativ sein [Es gibt " + gameConfig.getNumberOfCatRideUsages() +" Katzenköder]" );
        }

        // Die angegebene Anzahl an Ködern darf nicht negativ sein
        if (gameConfig.getNumberOfMouseRideUsages()<0){ 
            throw new IllegalArgumentException("Anzahl der Mausköder darf nicht negativ sein [Es gibt " + gameConfig.getNumberOfMouseRideUsages() +" Mausköder]");
        }

        // Die angegebene Anzahl an Ködern darf nicht negativ sein
        if (gameConfig.getNumberOfOwlFlightUsages()<0){ 
            throw new IllegalArgumentException("Anzahl der Eulenköder darf nicht negativ sein [Es gibt " + gameConfig.getNumberOfOwlFlightUsages() +" Eulenköder]");
        }

        // Die angegebene Anzahl an Ködern darf nicht negativ sein
        if (gameConfig.getNumberOfSecretUsages()<0){ 
            throw new IllegalArgumentException("Anzahl der Geheimpfadköder darf nicht negativ sein [Es gibt " + gameConfig.getNumberOfSecretUsages() +" Geheimpfadköder]");
        }

        // Die Anzahl an Startfeldern und Spielern muss gleich sein
        if (startPostions.size()>parser.getPlayerNames().size()){ 
          int abstand = gameConfig.getStartPositions().size()-parser.getPlayerNames().size();
          throw new IllegalArgumentException("Es gibt mehr Startfelder als Spieler! [Es gibt " + abstand+ " Startfelder zu viel]");
        }

        // Die Anzahl an Startfeldern und Spielern muss gleich sein
        if (startPostions.size()<parser.getPlayerNames().size()){ 
            int abstand = parser.getPlayerNames().size()-gameConfig.getStartPositions().size();
            throw new IllegalArgumentException("Es gibt weniger Startfelder als Spieler! [Es gibt " + abstand+ " Startfelder zu wenig]");
        }

        // Startfelder dürfen nicht mehrmals bestzt werden
        for (int x=0;x<startPostions.size();x++){ 
            for (int y=0;y<startPostions.size();y++){ 
                // Überprüfe wie oft jedes Startfelder in der Liste aller Startfelder vorkommt 
                if (startPostions.get(x).equals(startPostions.get(y))){ 
                     count++;
                    }
                    // Ein Startfeld darf nicht mehr als einmal besetzt werden
                    if (count>1){ 
                        throw new IllegalArgumentException("Startfelder dürfen nicht mehrmals besetzt werden! [Fehler bei Feld: (" + startPostions.get(x).getColumn() + "/"
                        + startPostions.get(x).getRow() + ")]");        
                }
            }
         count=0;             
        }
    }

    /**
     * Interpretiert das Board aus der Spielkonfiguration und sucht Waldfelder und die verbundenen Pfade.
     * 
     * @param gameConfig Spielekonfiguration um das Board zu erhalten
     * @param paths      Set von paths in welche die gefundenen Pfade gespeichert werden
     * @param fields     Set von Felder in welchen die Waldfelder gespeichert werden 
     * 
     * @throws   IllegalArgumentException wenn Waldfelder mehrfach vorkommen oder wenn Waldfelder nicht mit anderen Waldfeldern verbunden sind 
     */    
    public static void parseBoard (GameConfiguration gameConfig, Set<Path> paths, Set<Position> fields) throws IllegalArgumentException{
        String board = gameConfig.getBoard();
        int pointer = 0;
        String field="";
        boolean found = false;

        // Gehe jedes Zeichen in der ersten Zeile der Spielkonfiguration durch
        while (pointer < board.length()) { 
            field = "";
            Iterator<Position> it;
            Position next;

            // Durchlaufe den String bis ":" als Trennzeichen für ein Waldfeld gefunden wird, entnehme und speichere dabei das Waldfeld
            while (!found) { 
                if (board.charAt(pointer + 1) == ':') 
                    found = true;
                field += board.charAt(pointer); 
                pointer++;
            }
    
            it = fields.iterator();
        
            // Prüfe ob Waldfelder mehrfach vorkommen
            while (it.hasNext()){ 
                next = it.next();
                if (next.toString().equals(field)){ 
                        throw new IllegalArgumentException("Waldfelder dürfen nicht mehrfach vorkommen! [Fehler bei Feld: " + field + "]");
                }
                
            }
            
            // Füge das Waldfeld in die Liste aller Waldfelder hinzu 
            fields.add(Position.parsePosition(field));
            
            pointer++;
            found = false;

            pointer = interpret (false, board, field, paths, pointer, PathType.MOUSE_RIDE);
        }     
    }

    
    /**
     * Erzeugt aus einem String mit Zielfeldern, einem Anfangsfeld und einem
     * Pfadtypen Pfade auf dem Spielbrett und speichert diese in einem Set von Pfaden.
     * 
     * @param path     String mit Zielfeldern
     * @param position Position des Anfangsfeldes
     * @param type     PathType der zu erzeugenden Pfade
     * @param paths    Set von Pfaden in welche die Pfade eingetragen werden
     */
    private static void parsePaths(String path, Position position, PathType type, Set<Path> paths) {
        
        // Besteht 'path' aus "{}", gibt es keine Pfade
        if (path.length() == 2) {
            return;
        }

        int pointer = 1;
        String field = "";
        boolean found = false;

        // Durchlaufe den String bis "," oder "}" als Trennzeichen für einen Pfad gefunden wird, entnehme und speichere dabei die einzelenen Pfade
        while (pointer < path.length()) {
            while (!found) {
                if (path.charAt(pointer + 1) == ',' || path.charAt(pointer + 1) == '}')
                    found = true;
                    
                field += path.charAt(pointer);
                pointer++;
            }

            // Füge den gefundenen Pfad der Liste der Pfade hinzu
            paths.add(new Path(position, Position.parsePosition(field), type));
            pointer++;
            field = "";
            found = false;
        }

        return;
    }

    /**
     * Interpretiert die einzelnen Pfade aus dem Board. 
     * 
     * @param pathFound wurde für ein Waldfeld ein Pfad gefunden
     * @param board     die erste Zeile der Spielkonfiguration als String
     * @param field     Aktuelles Waldfeld
     * @param paths     List von allen Pfaden
     * @param pointer   Zeiger, um auf einzelne Zeichen im String zuzugreifen  
     * @param type      Pfadtyp für den aktuell eingelesen Abschnitt im String
     * 
     * @return pointer  Zeiger auf das aktuelle Zeichen
     * 
     * @throws IllegalArgumentException wenn Waldfeld keine Verbindung zu anderen Waldfeldern besitzt
     */
    public static int interpret(Boolean pathFound, String board, String field, Set<Path> paths, int pointer , PathType type) throws IllegalArgumentException{
       
        boolean found=false;
        String path="";
        
        // Iteriere die Methode vier mal, da es insgesamt vier Pfadtypen für jedes Waldfeld gibt 
        for (int i=0;i<4;i++){
            // Durchlaufe den String bis ":" oder ";" als Trennzeichen für einen Pfad gefunden wird oder das Ende des Strings erreicht, notiere dabei die einzelnen Pfade
            while (!found) {
                if ((pointer+1)==board.length() || board.charAt(pointer + 1) == ':' || board.charAt(pointer + 1) == ';' ){ 
                    found = true;
                }   
                path += board.charAt(pointer); 
                pointer++;
            
            }
        
            // Wenn der gefundene Pfad keinen leeren Inhalt hat, wurde ein Pfad gefunden
            if (!path.equals("{}")){
                pathFound=true;
            }

            // Füge den Pfad mit dem jeweiligen Pfadtyp hinzu
            parsePaths(path, Position.parsePosition(field), PathType.values()[type.ordinal()+i], paths);
            pointer++;
            path = "";
            found = false;
    }
        // Sollte ein Waldfeld keine Verbindungen zu anderen Waldfeldern besitzen ist dieses ungültig
        if (pathFound==false){ 
            throw new IllegalArgumentException("Waldfelder müssen mit anderen Waldfeldern verbunden sein! [Fehler bei Feld: " + field + "]");        
        }

        // Interpretier die Pfade für den nächsten Pfadtyp
        return pointer;
    }

    /**
     * Liefert das mit der gültigen Spielkonfiguration erstellte Spielbrett zurück
     * 
     * @return gameBoard gültiges Spielbrett
     */
    public GameBoard getGameBoard (){
        return gameBoard;
    }

}