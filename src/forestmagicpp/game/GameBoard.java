package forestmagicpp.game;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import forestmagicpp.preset.GameConfiguration;
import forestmagicpp.preset.IllegalMoveException;
import forestmagicpp.preset.PathType;
import forestmagicpp.preset.Position;
import forestmagicpp.preset.Viewer;
import forestmagicpp.preset.Viewable;
import forestmagicpp.preset.customgraphics.Path;
import forestmagicpp.preset.move.Move;

/**
 * Spielbrett des Forest Magic Spiels. Beinhaltet den Wald und kann Spielzüge
 * entgegennehmen und ausführen. Wird mit einer GameConfiguration erzeugt.
 * 
 * @author Daniel Wiedenmeier, Lasslo Steininger
 */
public class GameBoard implements Viewer, Viewable {
    /** Spielkonfiguration */
    private GameConfiguration gameConfig;

    /** Liste aller Spielfelder */
    private Set<Position> fields = new HashSet<>();

    /** Liste aller Pfade */
    private Set<Path> paths = new HashSet<>();

    /** Liste der vorhandenen Mäuse Köder */
    private List<Integer> MOUSE_TICKETS = new ArrayList<>(0);

    /** Liste der vorhandenen Katzen Köder */
    private List<Integer> CAT_TICKETS = new ArrayList<>(0);

    /** Liste der vorhandenen Eulen Köder */
    private List<Integer> OWL_TICKETS = new ArrayList<>(0);

    /** Liste der vorhandenen Geheimpfad Köder */
    private List<Integer> SECRET_TICKETS = new ArrayList<>(0);

    /** Liste der Positionen der Spieler */
    private List<Position> playerPositions = new ArrayList<>(0);

    /** Status der Spieler (im Spiel / ausgeschieden) */
    private List<Boolean> eliminated = new ArrayList<>(0);

    /** List der Züge des Goblins */
    private ArrayList<PathType> goblinMoves = new ArrayList<>(0);

    /** Aktuelle Runde */
    private int round = 1;

    /** Nummer des aktuellen Spielers */
    private int activePlayerNumber = 0;

    /** Anzahl der Spieler */
    private int playerCount = 0;

    /** Status des Spiels */
    private boolean gameIsRunning = true;

    /**
     * Erzeugt ein GameBoard mit einer Spielkonfiguration
     * 
     * @param gameConfiguration Spielkonfiguration
     */    
    public GameBoard(GameConfiguration gameConfiguration) {
        this.gameConfig = gameConfiguration;
        this.playerCount = gameConfiguration.getStartPositions().size();
        Iterator<Position> it = gameConfig.getStartPositions().iterator();
        int i = 0;

        // Initialisiere die Position der Spieler mit den Startpositionen
        while (i < playerCount && it.hasNext()) { 
            playerPositions.add(it.next());
            i++;
        }
        
        // Erstelle mit der geprüften Spielkonfiguration die Spielfelder und Pfade
        BoardGenerator.parseBoard(gameConfig,paths,fields); 
        // Verteile die Köder an die Spieler 
        giveTickets(); 
        // Initialisiere den Status jeden Spielers
        initEliminated(); 
    }
    
    /**
     * Verteilt die in der Spielkonfiguration festgelgte Anzahl an Ködern an die Spieler
     * 
     */
    private void giveTickets(){
        
        // Initialsiere die Köder des Goblins mit -1
        CAT_TICKETS.add(-1); 
        MOUSE_TICKETS.add(-1);
        OWL_TICKETS.add(-1);
        SECRET_TICKETS.add(gameConfig.getNumberOfSecretUsages());
 
        // Verteile an jeden Feen Spieler die in der Spielkonfiguration festgelegten Köder
        for (int i = 1; i < playerCount; i++)
            CAT_TICKETS.add(gameConfig.getNumberOfCatRideUsages());

        for (int i = 1; i < playerCount; i++)
            MOUSE_TICKETS.add(gameConfig.getNumberOfMouseRideUsages());

        for (int i = 1; i < playerCount; i++)
            OWL_TICKETS.add(gameConfig.getNumberOfOwlFlightUsages());

        for (int i = 1; i < playerCount; i++)
            SECRET_TICKETS.add(0);
    }

    @Override
    public int getNumberOfFairies() {
        return playerCount - 1;
    }

    @Override
    public int getActivePlayerNumber() {
        return activePlayerNumber;
    }
    
    @Override
    public int getNumberOfAvailableLinkTypeUsages(PathType type) throws IllegalArgumentException {
        // Gibt die Anzahl an Geheimpfad Köder zurück
        if (type.equals(PathType.SECRET_TUNNEL))
            return SECRET_TICKETS.get(activePlayerNumber);
        
        // Der Goblins hat maximal viele Köder zur Verfügung (ausser für den Geheimpfad)
        if (activePlayerNumber == 0)
            return Integer.MAX_VALUE;
        
        // Gibt die Anzahl an Maus Köder zurück
        if (type.equals(PathType.MOUSE_RIDE))
            return MOUSE_TICKETS.get(activePlayerNumber);

        // Gibt die Anzahl an Katzen Köder zurück
        if (type.equals(PathType.CAT_RIDE))
            return CAT_TICKETS.get(activePlayerNumber);

        // Gibt die Anzahl an Eulen Köder zurück    
        if (type.equals(PathType.OWL_FLIGHT))
            return OWL_TICKETS.get(activePlayerNumber);

        throw new IllegalArgumentException();
    }

    @Override
    public Position getPositionOfPlayer(int playerNumber) throws IllegalArgumentException {
        // Prüfe ob die Spielernummer gültig ist, also weder negativ noch größer als die festgelegte Spieleranzahl
        if (playerNumber < 0 || playerNumber >= playerCount) 
            throw new IllegalArgumentException();

        return playerPositions.get(playerNumber);
    }

    @Override
    public Set<Position> getBoardPositions() {
        return fields;
    }

    @Override  
    public Set<PathType> getPositionTypes(Position position)  throws IllegalArgumentException {
        // Übergebene Position muss ein Waldfeld sein
        if (!fields.contains(position)) 
            throw new IllegalArgumentException("Diese Position existiert nicht");

        Set<PathType> pathtypes = new HashSet<>();
        Iterator<Path> iterator = paths.iterator();
        Path path;

        // Durchlaufe alle Pfade und schaue welche Pfade die aktuelle Position des Spielers enthalten
        while (iterator.hasNext()) { 
            path = iterator.next();
            // Nimm den Pfadtyp an der Position des Spielers in eine Liste auf, falls dieser noch nicht aufgenommen wurde
            if ((path.getA().equals(position) || path.getB().equals(position)) && !pathtypes.contains(path.getType())) 
                pathtypes.add(path.getType());
        }

        return pathtypes;
    }

    @Override
    public Set<Position> getPositionLinks(Position position, PathType type) throws IllegalArgumentException {
        // Übergebene Position muss ein Waldfeld sein
        if (!fields.contains(position))
            throw new IllegalArgumentException("Diese Position existiert nicht");

        // Übergebener Pfadtyp muss ein gültiger Pfadtyp sein
        if (!type.equals(PathType.MOUSE_RIDE) && 
            !type.equals(PathType.CAT_RIDE) && 
            !type.equals(PathType.OWL_FLIGHT) && 
            !type.equals(PathType.SECRET_TUNNEL)) { 
            
            throw new IllegalArgumentException("Dieser Pfadtyp existiert nicht");
        }

        Set<Position> positionlinks = new HashSet<>();
        Iterator<Path> iterator = paths.iterator();
        Path path;
        
        // Durchlaufe alle Pfade und schaue welche Pfade die aktuelle Position des Spielers enthalten
        while (iterator.hasNext()) { 
            path = iterator.next();

            // Ist ein Pfad eine Verbindung zu der übergebenen Position wird dies in eine List mitaufgenommen
            if ((path.getA().equals(position) || path.getB().equals(position)) && path.getType().equals(type)) 
                if (position.equals(path.getA()))
                    positionlinks.add(path.getB());
                else
                    positionlinks.add(path.getA());
        }

        return positionlinks;
    }

    @Override
    public Set<Move> getPossibleMoves() { 
        Position position = playerPositions.get(activePlayerNumber);
        Set<Move> possibleMoves = new HashSet<>();
        Iterator<Path> iterator = paths.iterator();
        Path path;
        Position destination;

        // Durchlaufe alle Pfade und schaue welche Pfade die aktuelle Position des Spielers enthalten
        while (iterator.hasNext()) {
            path = iterator.next();

            if ((path.getA().equals(position) || path.getB().equals(position)) && getNumberOfAvailableLinkTypeUsages(path.getType()) > 0) {
                if (position.equals(path.getA()))
                    destination = path.getB();
                else
                    destination = path.getA();

                // Überprüfe für Feen ob auf dem Zielfeld keine Fee steht
                boolean check = true;

                if (activePlayerNumber != 0)
                    for (int i = 1; i < playerCount; i++)
                        if (i != activePlayerNumber && playerPositions.get(i).equals(destination))
                            check = false;

                // Besitzt der Spieler ausreichend Köder für den Pfad und ist keine andere Fee auf dem Zielpfade, nehm den Pfad auf als möglichen Pfad
                if (check) { 
                    possibleMoves.add(new Move(position, destination, path.getType()));

                    if (activePlayerNumber == 0 && getNumberOfAvailableLinkTypeUsages(PathType.SECRET_TUNNEL) > 0)
                        possibleMoves.add(new Move(position, destination, PathType.SECRET_TUNNEL));
                }
            }
        }

        // Falls eine Fee keinen Zug machen kann, muss sie passen und scheidet aus
        if (possibleMoves.isEmpty() || eliminated.get(activePlayerNumber)) {
            possibleMoves.clear();
            possibleMoves.add(Move.PASS);
            eliminated.set(activePlayerNumber, true);
        }

        return possibleMoves;
    }

    @Override
    public PathType getGoblinMoveTypeFromRound(int round)  throws java.lang.IllegalArgumentException {
       
        // Überprüfe ob die gewählte Runde nicht negativ ist und gespielt wurde
        if (round <= 0 || round > goblinMoves.size())
            throw new IllegalArgumentException();
        
        return goblinMoves.get(round - 1);
    }

    @Override
    public int getRound() {
        return round;
    }

    @Override
    public Set<Integer> getVisibleRounds() {
        return gameConfig.getVisibleRounds();
    }

    @Override
    public int getMaxNumberOfRounds() {
        return gameConfig.getMaxNumberOfRounds();
    }

    @Override
    public Viewer viewer() {
        return this;
    }

    /** 
     * Aktuallisiere das GameBoard abhängig von dem Zug eines Spielers.
     * 
     * @param  move der Zug des aktuellen Spielers
     *     
     * @throws IllegalArgumentException wenn der Zug kein gültiger Zug ist und der Zug nicht von einem Goblin durgeführt wird
     *             
     */  
    public void update(Move move) throws IllegalMoveException {
        
        // Ist das Spiel vorbei, so kann kein Zug durchgeführt werden
        if (!gameIsRunning)
            throw new IllegalStateException();

        // Züge von Feen müssen allgemein gültige Züge sein, ausser die Startposition und Endposition des Zuges sind unbekannt, dann handelt es sich beim Spieler um den Goblin        
        if (getPossibleMoves().contains(move) || 
            move.getSource().equals(Position.UNKNOWN) ||
            move.getDestination().equals(Position.UNKNOWN)) { 
            if (!move.getDestination().equals(Position.UNKNOWN) || activePlayerNumber == 0){
                // Aktualisiere die Position des Spielers, abhängig vom Zug
                playerPositions.set(activePlayerNumber, move.getDestination());
            }

            // Speicher den Zug des Goblins für die Methode "getGoblinMoveTypeFromRound"
            if (activePlayerNumber == 0){ 
                goblinMoves.add(move.getPath());
            }    

            // Entnehme dem Spieler für den Zug einen von den jeweiligen Ködern
            takeTickets(move.getPath());
            
            // Der nächste Spieler ist am Zug
            activePlayerNumber = ++activePlayerNumber % playerCount; 

            // Sobald jeder Spieler einen Zug in einer Runde durchgeführt hat, beginnt die nächste Runde
            if (activePlayerNumber == 0) 
                round++;
            
            // Ist der Zug ungültig, so wird das Spiel abgebrochen
            } else { 
            gameIsRunning = false; 
            throw new IllegalMoveException("Dieser Zug ist ungültig!");
        }
        
        // Prüfe ob die Rundenanzahl überschritten wird 
        if (round > gameConfig.getMaxNumberOfRounds()){ 
            gameIsRunning = false;
        }
    }

    /**
     * Entnimmt dem Spieler einen Köder abhängig von dem gewählten Pfad.
     * 
     * @param type Pfadtyp des gewählten Zugs
     */
    private void takeTickets (PathType type){
        // Ziehe dem Spieler einen Köder ab, welcher dem gewählten Pfadtyp entspricht
        switch (type) {     
            case MOUSE_RIDE:
                MOUSE_TICKETS.set(activePlayerNumber, MOUSE_TICKETS.get(activePlayerNumber) - 1);
                break;

            case CAT_RIDE:
                CAT_TICKETS.set(activePlayerNumber, CAT_TICKETS.get(activePlayerNumber) - 1);
                break;
        
            case OWL_FLIGHT:
                OWL_TICKETS.set(activePlayerNumber, OWL_TICKETS.get(activePlayerNumber) - 1);
                break;

            case SECRET_TUNNEL:
                SECRET_TICKETS.set(activePlayerNumber, SECRET_TICKETS.get(activePlayerNumber) - 1);
                break;
        }
    }

    /** 
     * Initialisiert die Liste der ausgeschiedenen Spieler damit, dass jeder Spieler noch im Spiel ist.
     */  
    public void initEliminated() {
        for (int i=0;i<playerCount;i++)
            eliminated.add(i, false);
    }

    /**
     * Liefert zurück ob das Spiel noch läuft.
     * 
     * @return gameIsRunning 
     */  
    public boolean isGameRunning() {
        return gameIsRunning;
    }

    /** 
     * Der Status des Spiels wird auf beendet gesetzt.
     */  
    public void endGame() {
        gameIsRunning = false;
    }
}
