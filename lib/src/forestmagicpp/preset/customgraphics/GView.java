package forestmagicpp.preset.customgraphics;

import forestmagicpp.preset.GameStatus;
import forestmagicpp.preset.GameView;
import forestmagicpp.preset.PathType;
import forestmagicpp.preset.PlayerRole;
import forestmagicpp.preset.Position;
import forestmagicpp.preset.Viewer;
import forestmagicpp.preset.graphics.GContainer;
import forestmagicpp.preset.graphics.GObject;
import forestmagicpp.preset.graphics.GWindow;
import forestmagicpp.preset.graphics.primitives.GCircle;
import forestmagicpp.preset.graphics.primitives.GImage;
import forestmagicpp.preset.graphics.primitives.GLine;
import forestmagicpp.preset.graphics.primitives.GText;
import forestmagicpp.preset.move.Move;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.Point2D;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class GView extends GWindow implements GameView {
    public static final String DEFAULT_TITLE = "ForestMagicPP";
    private static final boolean SIMPLE = false;

    private GText title;

    private MoveBuilder moveBuilder = new MoveBuilder();
    private boolean inputEnabled = false;
    private Viewer viewer;

    private GFadeContainer gameStatus = new GFadeContainer(false);
    private GText gameStatusMessage;

    private GFadeContainer tickets = new GFadeContainer(false);
    private Map<PathType, GTicketIndicator> ticketMap = new HashMap<>();

    private Map<Integer, GRoundIndicator> roundIndicators = new HashMap<>();
    private GContainer roundIndicatorContainer = new GContainer();

    private GContainer board = new GContainer();
    private Map<Path, GPath> paths = new HashMap<>();
    private Map<Path, GPathSegment> pathSegments = new HashMap<>();
    private Map<Position, GField> fields = new HashMap<>();
    private GFadeContainer tokenContainer = new GFadeContainer();
    private Map<Integer, GPlayerToken> tokens = new HashMap<>();

    // History
    private GFadeContainer historyTokenContainer = new GFadeContainer(false);
    private Map<Integer, GPlayerToken> historyTokens = new HashMap<>();
    private List<Position> historyPositions = new LinkedList<>();

    private double scaling = GameGraphicConstants.BOARD_DEFAULT_SCALING;
    private boolean activeRendering = true;

    private boolean pathAlignment = false;

    public GView() {
        this(true);
    }

    public GView(boolean activeRendering) {
        super(DEFAULT_TITLE);
        setSize(800, 450);
        //        setExtendedState(MAXIMIZED_BOTH);
        //        setupKeyboardCallbacks();
        //        setupWindowCallbacks();
        createBackground();
        createTitle();
        createBoard();
        createRoundIndicators();
        createAndSetupTicketIndicators();
        setupGameText();

        scale(scaling);

        setActiveRendering(activeRendering);
        singleUpdate();
    }

    @Override
    public void setActiveRendering(boolean value) {
        this.activeRendering = value;
        super.setActiveRendering(value);
    }

    @Override
    public void setPathAlignment(boolean enabled) {
        this.pathAlignment = enabled;
    }

    //    private void setupWindowCallbacks() {
    //        setOnFocusGainedCallback(() -> setUpdateRate(50));
    //        setOnFocusLostCallback(() -> setUpdateRate(2));
    //    }
    //
    //    private void setupKeyboardCallbacks() {
    //        setKeyboardCallback(keyboardInfo -> {
    //            if (keyboardInfo.getState() == KeyboardState.RELEASED) {
    //                return;
    //            }
    //            if (keyboardInfo.getKeyCode() == KeyEvent.VK_ADD) {
    //                if (scaling < GameGraphicConstants.BOARD_MAX_SCALING) {
    //                    scaling *= 1.01;
    //                    scale(scaling);
    //                }
    //
    //            } else if (keyboardInfo.getKeyCode() == KeyEvent.VK_SUBTRACT) {
    //                if (scaling > GameGraphicConstants.BOARD_MIN_SCALING) {
    //                    scaling *= 0.99;
    //                    scale(scaling);
    //                }
    //            }
    //        });
    //    }

    private void createBackground() {
        if (!SIMPLE) {
            try {
                GImage background = new GImage(ImageIO.read(getClass().getResource("/background.png")));
                background.scale(2.0, 2.0);
                add(background);
            } catch (IOException e) {
                System.err.println("Unable to create background!");
            }
        }
    }

    public void setTitle(String title) {
        if (this.title != null) {
            super.setTitle(title);
            this.title.setText(title);
        }
    }

    private void createTitle() {
        if (!SIMPLE) {
            title = new GText();
            title.setText(DEFAULT_TITLE);
            title.setTextPaint(GameGraphicConstants.GAME_STATUS_MESSAGE);
            title.setAutoCenter(false);
            title.setAutoScale(false);
            title.translate(-0.95, 0.8);
            title.setOutlinePaint(Color.BLACK);
            title.scale(0.1, 0.1 * 16 / 9);
            title.setStroke(new BasicStroke(0.015f));
            try {
                Font f = Font.createFont(Font.TRUETYPE_FONT, getClass().getResourceAsStream("/PrinceValiant.ttf"));
                title.setFont(f);
            } catch (FontFormatException e) {
                System.err.println("Unable to load font");
            } catch (IOException e) {
                System.err.println("Unable to load font");
            }
            add(title);
        }
    }

    private void createBoard() {
        board.translate(0.1, 0.0);
        add(board);
    }

    private void createRoundIndicators() {
        roundIndicatorContainer.translate(-0.9, -0.9);
        add(roundIndicatorContainer);
    }

    private void createAndSetupTicketIndicators() {
        GTicketIndicator mouseRideTicketIndicator = new GTicketIndicator(PathType.MOUSE_RIDE, this::clickTicket);
        GTicketIndicator catRideTicketIndicator = new GTicketIndicator(PathType.CAT_RIDE, this::clickTicket);
        GTicketIndicator owlFlightTicketIndicator = new GTicketIndicator(PathType.OWL_FLIGHT, this::clickTicket);
        GTicketIndicator secretTicketIndicator = new GTicketIndicator(PathType.SECRET_TUNNEL, this::clickTicket);

        tickets.add(mouseRideTicketIndicator);
        tickets.add(catRideTicketIndicator);
        tickets.add(owlFlightTicketIndicator);
        tickets.add(secretTicketIndicator);
        mouseRideTicketIndicator.translate(0.0, 1.5);
        catRideTicketIndicator.translate(0.0, 0.5);
        owlFlightTicketIndicator.translate(0.0, -0.5);
        secretTicketIndicator.translate(0.0, -1.5);
        tickets.translate(-0.83, 0.0);
        tickets.scale(0.15, 0.15 * 16 / 9);
        tickets.setFadeSpeed(3);

        ticketMap.put(PathType.MOUSE_RIDE, mouseRideTicketIndicator);
        ticketMap.put(PathType.CAT_RIDE, catRideTicketIndicator);
        ticketMap.put(PathType.OWL_FLIGHT, owlFlightTicketIndicator);
        ticketMap.put(PathType.SECRET_TUNNEL, secretTicketIndicator);

        add(tickets);
    }

    private void setupGameText() {
        try {
            GImage banner = new GImage(ImageIO.read(getClass().getResource("/banner.png")));
            banner.translate(0.0, -0.1);
            banner.scale(1.5, 1.0);
            gameStatus.add(banner);
        } catch (IOException e) {
            System.err.println("Unable to load banner");
        }

        gameStatusMessage = new GText();
        gameStatusMessage.setTextPaint(GameGraphicConstants.GAME_STATUS_MESSAGE);
        gameStatusMessage.setOutlinePaint(Color.BLACK);
        gameStatusMessage.scale(1.0, 1.0 * 16 / 9);
        gameStatusMessage.setStroke(new BasicStroke(0.01f));
        try {
            gameStatusMessage.setFont(
                    Font.createFont(Font.TRUETYPE_FONT, getClass().getResourceAsStream("/PrinceValiant.ttf")));
        } catch (FontFormatException e) {
            System.err.println("Unable to load font");
        } catch (IOException e) {
            System.err.println("Unable to load font");
        }

        gameStatus.add(gameStatusMessage);
        gameStatus.setFadeSpeed(2);

        add(gameStatus);
    }

    @Override
    public void setViewer(final Viewer viewer) {
        this.viewer = viewer;
        clearGameElements();
        try {
            board.add(createBoardObjects());
        } catch (RuntimeException e) {
            System.err.println("Unable to set viewer! An exception occurred!");
            e.printStackTrace();
        }
        for (int i = 0; i <= viewer.getNumberOfFairies(); i++) {
            Position playerPosition = viewer.getPositionOfPlayer(i);
            tokens.get(i)
                  .setPosition(playerPosition);
            fields.get(playerPosition)
                  .showBig(true);
        }
        GContainer tempRoundIndicatorContainer = new GContainer();
        Set<Integer> visibleRounds = this.viewer.getVisibleRounds();
        for (int i = 1; i <= viewer.getMaxNumberOfRounds(); i++) {
            GRoundIndicator roundIndicator = new GRoundIndicator(i, this.viewer.getMaxNumberOfRounds(),
                    visibleRounds.contains(i));
            roundIndicators.put(i, roundIndicator);
            final int j = i;
            //            roundIndicator.setOnMouseEnterCallback(() -> showRoundHistory(j));
            //            roundIndicator.setOnMouseLeaveCallback(() -> showRoundHistory(-1));
            tempRoundIndicatorContainer.add(roundIndicator);
        }
        double scaleConstant = 1.8 / (this.viewer.getMaxNumberOfRounds() - 1);
        roundIndicatorContainer.scale(scaleConstant, scaleConstant * 16 / 9);
        roundIndicatorContainer.add(tempRoundIndicatorContainer);
        gameStatus.hide();

        singleUpdate();

        setVisible(true);
    }

    private void clearGameElements() {
        board.clear();
        tokenContainer.clear();
        tokens.clear();
        historyTokenContainer.clear();
        historyTokens.clear();
        historyPositions.clear();
        fields.clear();
        paths.clear();
        pathSegments.clear();
        roundIndicatorContainer.clear();
        roundIndicators.clear();
    }

    @Override
    public synchronized Move request() {
        moveBuilder.reset();
        moveBuilder.setSource(viewer.getPositionOfPlayer(viewer.getActivePlayerNumber()));
        Set<Move> possibleMoves = viewer.getPossibleMoves();
        moveBuilder.setPossibleMoves(possibleMoves);

        inputEnabled = true;
        for (PathType type : PathType.values()) {
            GTicketIndicator indicator = ticketMap.get(type);
            int amount = this.viewer.getNumberOfAvailableLinkTypeUsages(type);
            indicator.setAmount(amount);
            if (amount > 0 && possibleMoves.stream()
                                           .map(m -> m.getPath())
                                           .filter(p -> p == type)
                                           .count() > 0) {
                indicator.setInteractable(true);
                indicator.setDefaultLevel(1);
            } else {
                indicator.setInteractable(false);
                indicator.setDefaultLevel(0);
            }
        }
        tickets.show();
        fields.values()
              .forEach(f -> {
                  f.setInteractable(false);
                  f.setDefaultLevel(0);
              });
        fields.get(moveBuilder.getSource())
              .setDefaultLevel(1);
        paths.values()
             .forEach(p -> p.setHighlighted(false));


        highlightToken(viewer.getActivePlayerNumber(), true);

        singleUpdate();

        //        while (!moveBuilder.isReady()) {
        try {
            wait();
        } catch (InterruptedException e) {

        }
        //        }

        highlightToken(viewer.getActivePlayerNumber(), false);
        paths.values()
             .forEach(p -> p.setHighlighted(true));
        fields.values()
              .forEach(f -> {
                  f.setInteractable(false);
                  f.setDefaultLevel(1);
              });
        ticketMap.values()
                 .forEach(indicator -> indicator.setInteractable(false));
        tickets.hide();
        inputEnabled = false;
        fields.values()
              .forEach(f -> f.setInteractable(false));

        singleUpdate();

        return moveBuilder.build();
    }

    private synchronized void clickField(Position position) {
        if (!inputEnabled) {
            return;
        }
        moveBuilder.setDestination(position);

        singleUpdate();

        if (moveBuilder.isReady()) {
            notify();
        }
    }

    private synchronized void clickTicket(PathType type) {
        if (!inputEnabled) {
            return;
        }

        moveBuilder.setPath(type);

        Set<Position> potentialDestinations = moveBuilder.getPossibleMoves()
                                                         .stream()
                                                         .map(m -> m.getDestination())
                                                         .collect(Collectors.toSet());
        fields.values()
              .forEach(f -> {
                  f.setInteractable(false);
                  f.setDefaultLevel(0);
              });
        fields.get(moveBuilder.getSource())
              .setDefaultLevel(1);
        paths.values()
             .forEach(p -> p.setHighlighted(false));
        if (type == PathType.SECRET_TUNNEL) {
            for (PathType t : PathType.values()) {
                moveBuilder.getPossibleMoves()
                           .stream()
                           .map(m -> new Path(m.getSource(), m.getDestination(), t))
                           .filter(p -> paths.containsKey(p))
                           .forEach(p -> paths.get(p)
                                              .setHighlighted(true));
            }
        } else {
            moveBuilder.getPossibleMoves()
                       .stream()
                       .map(m -> new Path(m.getSource(), m.getDestination(), type))
                       .filter(p -> paths.containsKey(p))
                       .forEach(p -> paths.get(p)
                                          .setHighlighted(true));
        }
        potentialDestinations.stream()
                             .forEach(d -> {
                                 fields.get(d)
                                       .setInteractable(true);
                                 fields.get(d)
                                       .highlightPath(type);
                             });

        for (PathType path : PathType.values()) {
            GTicketIndicator indicator = ticketMap.get(path);
            if (path == type) {
                indicator.setDefaultLevel(2);
            } else {
                indicator.setDefaultLevel(0);
            }
        }

        singleUpdate();
    }

    private void showRoundHistory(int round) {
        for (Map.Entry<Integer, GRoundIndicator> ri : roundIndicators.entrySet()) {
            ri.getValue()
              .setHighlighted(ri.getKey() == round);
        }

        int n = viewer.getNumberOfFairies() + 1;
        boolean showHistory = round > 0 && round * n <= historyPositions.size();
        if (showHistory) {
            historyTokenContainer.show();
            tokenContainer.hide();
            highlightToken(viewer.getActivePlayerNumber(), false);
            fields.values()
                  .forEach(f -> f.showBig(false));
            for (int i = 0; i < n; i++) {
                int index = i + (round - 1) * n;
                Position playerPosition = historyPositions.get(index);
                historyTokens.get(i)
                             .setPosition(playerPosition);
                fields.get(playerPosition)
                      .showBig(true);
            }
        } else {
            historyTokenContainer.hide();
            tokenContainer.show();
            highlightToken(viewer.getActivePlayerNumber(), true);
            fields.values()
                  .forEach(f -> f.showBig(false));
            for (int i = 0; i <= viewer.getNumberOfFairies(); i++) {
                Position playerPosition = viewer.getPositionOfPlayer(i);
                tokens.get(i)
                      .setPosition(playerPosition);
                fields.get(playerPosition)
                      .showBig(true);
            }
        }
    }

    private void highlightToken(int activePlayer, boolean value) {
        tokens.get(activePlayer)
              .setHighlighted(value);
    }

    @Override
    public void update(Move move) {
        if (tokens == null || fields == null) {
            return;
        }

        fields.values()
              .forEach(gf -> gf.showBig(false));

        int n = viewer.getNumberOfFairies() + 1;
        for (int i = 0; i < n; i++) {
            Position playerPosition = viewer.getPositionOfPlayer(i);
            if (fields.containsKey(playerPosition)) {
                fields.get(playerPosition)
                      .showBig(true);
            }
        }

        historyPositions.add(move.getSource());

        int movedToken = (viewer.getActivePlayerNumber() + n - 1) % n;
        GPlayerToken token = tokens.get(movedToken);

        // Hide, Show or Move
        if (!move.equals(Move.PASS)) {
            if (move.getDestination()
                    .equals(Position.UNKNOWN)) {
                token.hide();
            } else if (move.getSource()
                           .equals(Position.UNKNOWN)) {
                token.setPosition(move.getDestination());
                token.show();
            } else {
                if (activeRendering) {
                    token.moveTo(move.getDestination());
                } else {
                    token.setPosition(move.getDestination());
                }
            }
        }

        int round = this.viewer.getRound();
        if (round <= this.viewer.getMaxNumberOfRounds()) {
            roundIndicators.get(round)
                           .setActive(true);
        }
        if (round > 1) {
            roundIndicators.get(round - 1)
                           .setActive(false);
        }

        try {
            PathType goblinMoveType = this.viewer.getGoblinMoveTypeFromRound(round);
            roundIndicators.get(round)
                           .setUsedPath(goblinMoveType);
        } catch (IllegalArgumentException e) {

        }

        singleUpdate();
    }

    @Override
    public void showStatus(GameStatus status) {
        switch (status) {
            case FAIRIES_WIN:
                gameStatusMessage.setText("Fairies win!");
                this.gameStatus.show();
                break;
            case GOBLIN_WINS:
                gameStatusMessage.setText("Goblin wins!");
                this.gameStatus.show();
                break;
        }
    }

    private void scale(double scaling) {
        board.scale(scaling, scaling * 16 / 9);
    }

    private GObject createBoardObjects() {
        GContainer board = new GContainer();
        GContainer backgrounds = new GContainer();
        board.add(backgrounds);
        board.add(createLinks(backgrounds));
        board.add(createFields(backgrounds));
        board.add(createPlayerTokens());
        layoutMultiLinks();
        return board;
    }

    private GObject createLinks(GContainer backgrounds) {
        GContainer links = new GContainer();
        Set<Position> graph = viewer.getBoardPositions();
        for (Position from : graph) {
            for (Position to : viewer.getPositionLinks(from, PathType.SECRET_TUNNEL)) {
                createLink(backgrounds, links, PathType.SECRET_TUNNEL, from, to,
                        Arrays.asList(new PathSegment(from, to)));
            }
        }
        for (PathType type : PathType.values()) {
            if (type == PathType.MOUSE_RIDE) {
                for (Position from : graph) {
                    for (Position to : viewer.getPositionLinks(from, type)) {
                        createLink(backgrounds, links, type, from, to, Arrays.asList(new PathSegment(from, to)));
                    }
                }
            } else if (type == PathType.CAT_RIDE || type == PathType.OWL_FLIGHT) {
                for (Position from : graph) {
                    for (Position to : viewer.getPositionLinks(from, type)) {
                        if (pathAlignment) {
                            createLink(backgrounds, links, type, from, to, findShortestPath(graph, from, to, type));
                        } else {
                            createLink(backgrounds, links, type, from, to, Arrays.asList(new PathSegment(from, to)));
                        }
                    }
                }
            }
        }
        return links;
    }

    private void createLink(GContainer backgrounds, GContainer links, PathType type, Position from, Position to,
                            List<PathSegment> segments) {
        Path path = new Path(from, to, type);
        if (!paths.containsKey(path)) {
            List<GPathSegment> gPathSegments = new ArrayList<>(segments.size());
            for (PathSegment s : segments) {
                gPathSegments.add(createPathSegment(backgrounds, s, links, type));
            }
            GPath gPath = new GPath(gPathSegments);
            paths.put(path, gPath);
        }
    }

    private GPathSegment createPathSegment(GContainer backgrounds, PathSegment segment, GContainer links,
                                           PathType type) {
        GObject background = null;
        if (!SIMPLE) {
            background = createLinkBackground(segment.getFrom(), segment.getTo(), type);
            backgrounds.add(background);
        }
        GPathSegment gPathSegment = new GPathSegment(positionToPoint(segment.getFrom()),
                positionToPoint(segment.getTo()), type, background);
        //            paths.put(path, gPathSegment);
        pathSegments.put(new Path(segment.getFrom(), segment.getTo(), type), gPathSegment);
        links.add(gPathSegment);
        return gPathSegment;
    }

    private GObject createLinkBackground(Position from, Position to, PathType type) {
        GLine linkBackground = new GLine(positionToPoint(from), positionToPoint(to));
        linkBackground.setStroke(GameGraphicConstants.LINK_BACKGROUND_STROKES[type.ordinal()]);
        linkBackground.setOutlinePaint(GameGraphicConstants.BASE_COLOR);
        return linkBackground;
    }

    private GObject createFields(GContainer backgrounds) {
        GContainer fields = new GContainer();
        for (Position position : viewer.getBoardPositions()) {
            if (!SIMPLE) {
                backgrounds.add(createFieldBackground(position));
            }
            GField field = createField(position);
            fields.add(field);
            this.fields.put(position, field);
        }
        return fields;
    }

    private GObject createFieldBackground(Position position) {
        Point2D point = positionToPoint(position);
        GCircle fieldBackground = new GCircle();
        fieldBackground.translate(point.getX(), point.getY());
        fieldBackground.scale(0.07, 0.07);
        fieldBackground.setFillPaint(GameGraphicConstants.BASE_COLOR);
        return fieldBackground;
    }

    private GField createField(Position position) {
        List<PathType> types = new ArrayList<>(viewer.getPositionTypes(position));
        Point2D point = positionToPoint(position);
        return new GField(point.getX(), point.getY(), position, types, this::clickField);
    }

    private GObject createPlayerTokens() {
        GContainer tokens = new GContainer();

        tokenContainer.setFadeSpeed(5.0);
        historyTokenContainer.setFadeSpeed(5.0);
        GPlayerToken goblinToken = new GPlayerToken(PlayerRole.GOBLIN, this::positionToPoint);
        goblinToken.setPosition(viewer.getPositionOfPlayer(0));
        tokenContainer.add(goblinToken);
        this.tokens.put(0, goblinToken);
        for (int i = 1; i <= viewer.getNumberOfFairies(); i++) {
            GPlayerToken fairyToken = new GPlayerToken(PlayerRole.FAIRY, this::positionToPoint);
            fairyToken.setPosition(viewer.getPositionOfPlayer(i));
            this.tokenContainer.add(fairyToken);
            this.tokens.put(i, fairyToken);
        }

        GPlayerToken goblinHistoryToken = new GPlayerToken(PlayerRole.GOBLIN, this::positionToPoint);
        for (int i = 0; i <= viewer.getNumberOfFairies(); i++) {
            historyPositions.add(viewer.getPositionOfPlayer(i));
        }
        historyTokenContainer.add(goblinHistoryToken);
        this.historyTokens.put(0, goblinToken);
        for (int i = 1; i <= viewer.getNumberOfFairies(); i++) {
            GPlayerToken fairyToken = new GPlayerToken(PlayerRole.FAIRY, this::positionToPoint);
            this.historyTokenContainer.add(fairyToken);
            this.historyTokens.put(i, fairyToken);
        }

        tokens.add(tokenContainer);
        tokens.add(historyTokenContainer);
        return tokens;
    }

    private void layoutMultiLinks() {
        for (Position from : viewer.getBoardPositions()) {
            for (Position to : viewer.getBoardPositions()) {
                if (to.compareTo(from) < 0) {
                    continue;
                }
                List<Path> multiPaths = new ArrayList<>(4);
                for (PathType type : PathType.values()) {
                    Path path = new Path(from, to, type);
                    if (pathSegments.containsKey(path)) {
                        multiPaths.add(path);
                    }
                }

                double distance = 1.0;
                if (!multiPaths.isEmpty()) {
                    Collections.sort(multiPaths);
                    int n = multiPaths.size();
                    for (int i = 0; i < n; i++) {
                        double offsetFactor = i * distance - (n - 1) * distance / 2.0;
                        pathSegments.get(multiPaths.get(i))
                                    .setOffset(offsetFactor);
                    }
                }
            }
        }
    }

    public Point2D positionToPoint(Position position) {
        Point2D point = new Point2D.Double(
                GameGraphicConstants.X_CONVERSION_FACTOR * position.getColumn() / Position.MAX_COLUMN -
                GameGraphicConstants.X_CONVERSION_FACTOR_HALF,
                GameGraphicConstants.Y_CONVERSION_FACTOR * position.getRow() / Position.MAX_ROW -
                GameGraphicConstants.Y_CONVERSION_FACTOR_HALF);
        return point;
    }

    private List<PathSegment> findShortestPath(Set<Position> graph, Position start, Position end, PathType type) {
        List<PathSegment> segments = new LinkedList<>();
        Map<Position, Double> distance = new HashMap<>();
        Map<Position, Position> predecessor = new HashMap<>();
        List<Position> unvisited = new ArrayList<>(graph.size());

        initDijkstra(graph, start, distance, predecessor, unvisited);
        dijkstra(graph, start, end, type, distance, predecessor, unvisited);
        dijkstraShortestPath(segments, end, predecessor);
        return segments;
    }

    private void initDijkstra(Set<Position> graph, Position start, Map<Position, Double> distance,
                              Map<Position, Position> predecessor, List<Position> unvisited) {
        for (Position p : graph) {
            distance.put(p, Double.POSITIVE_INFINITY);
            predecessor.put(p, null);
        }
        distance.put(start, 0.0);
        unvisited.addAll(graph);
    }

    private void dijkstra(Set<Position> graph, Position start, Position end, PathType type,
                          Map<Position, Double> distance, Map<Position, Position> predecessor,
                          List<Position> unvisited) {
        while (!unvisited.isEmpty()) {
            double minDistance = distance.entrySet()
                                         .stream()
                                         .filter(e -> unvisited.contains(e.getKey()))
                                         .map(e -> e.getValue())
                                         .min(Double::compareTo)
                                         .get();
            // Hacky but should work!
            Position next = (Position) distance.entrySet()
                                               .stream()
                                               .filter(e -> unvisited.contains(e.getKey()))
                                               .filter(e -> e.getValue()
                                                             .equals(minDistance))
                                               .map(e -> e.getKey())
                                               .toArray()[0];
            unvisited.remove(next);
            if (next.equals(end)) {
                return;
            }
            for (Position neighbor : viewer.getPositionLinks(next, PathType.MOUSE_RIDE)) {
                Path p = new Path(next, neighbor, type);
                if (!pathSegments.containsKey(p) && unvisited.contains(neighbor)) {
                    dijkstra_distance_update(next, neighbor, distance, predecessor, 1.0);
                }
            }
            for (Position neighbor : graph) {
                Path p = new Path(next, neighbor, type);
                if (!pathSegments.containsKey(p) && unvisited.contains(neighbor)) {
                    dijkstra_distance_update(next, neighbor, distance, predecessor, 2.0);
                }
            }
        }
    }

    private void dijkstra_distance_update(Position u, Position v, Map<Position, Double> distance,
                                          Map<Position, Position> predecessor, double factor) {
        double alternative = distance.get(u) + factor * dijkstra_distance_between(u, v);
        if (alternative < distance.get(v)) {
            distance.put(v, alternative);
            predecessor.put(v, u);
        }
    }

    private double dijkstra_distance_between(Position u, Position v) {
        double a = u.getColumn() - v.getColumn();
        double b = u.getRow() - v.getRow();
        return Math.sqrt(a * a + b * b);
    }

    private void dijkstraShortestPath(List<PathSegment> segments, Position end, Map<Position, Position> predecessor) {
        List<Position> path = new LinkedList<>();
        path.add(end);
        Position u = end;
        while (predecessor.get(u) != null) {
            u = predecessor.get(u);
            path.add(0, u);
        }
        for (int i = 0; i < path.size() - 1; i++) {
            segments.add(new PathSegment(path.get(i), path.get(i + 1)));
        }
    }
}
