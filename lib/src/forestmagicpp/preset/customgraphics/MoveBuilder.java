package forestmagicpp.preset.customgraphics;

import forestmagicpp.preset.PathType;
import forestmagicpp.preset.Position;
import forestmagicpp.preset.move.Move;

import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

public class MoveBuilder {
    private Collection<Move> possibleMoves;
    private Position source;
    private Position destination;
    private PathType path;

    public MoveBuilder() {
        reset();
    }

    public void reset() {
        this.possibleMoves = null;
        this.source = null;
        this.destination = null;
        this.path = null;
    }

    public void setPossibleMoves(final Collection<Move> possibleMoves) {
        if (possibleMoves == null) {
            throw new IllegalArgumentException("possibleMoves == null");
        }
        this.possibleMoves = possibleMoves;
    }

    public Position getSource() {
        return source;
    }

    public void setSource(final Position source) {
        if (source == null) {
            throw new IllegalArgumentException("source == null");
        }
        this.source = source;
    }

    public Position getDestination() {
        return destination;
    }

    public void setDestination(final Position destination) {
        if (destination == null) {
            throw new IllegalArgumentException("destination == null");
        }
        this.destination = destination;
    }

    public PathType getPath() {
        return path;
    }

    public void setPath(final PathType path) {
        if (path == null) {
            throw new IllegalArgumentException("path == null");
        }
        this.path = path;
    }

    public boolean isReady() {
        return this.source != null && this.destination != null && this.path != null && this.possibleMoves != null &&
               this.possibleMoves.contains(build());
    }

    public Move build() {
        return new Move(this.source, this.destination, this.path);
    }

    public Collection<Move> getPossibleMoves() {
        if (this.path == null) {
            return Collections.emptySet();
        }
        return possibleMoves.stream()
                            .filter(move -> move.getPath() == this.path)
                            .collect(Collectors.toSet());
    }
}
