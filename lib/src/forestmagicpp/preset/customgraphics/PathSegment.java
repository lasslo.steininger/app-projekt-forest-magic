package forestmagicpp.preset.customgraphics;

import forestmagicpp.preset.Position;

public class PathSegment {
    private final Position from;
    private final Position to;

    public PathSegment(final Position from, final Position to) {
        if (from.compareTo(to) < 0) {
            this.from = from;
            this.to = to;
        } else {
            this.from = to;
            this.to = from;
        }
    }

    public Position getFrom() {
        return from;
    }

    public Position getTo() {
        return to;
    }
}
