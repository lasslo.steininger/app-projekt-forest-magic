package forestmagicpp.preset.customgraphics;

import forestmagicpp.preset.PathType;
import forestmagicpp.preset.graphics.GContainer;
import forestmagicpp.preset.graphics.TicketClickHandler;
import forestmagicpp.preset.graphics.primitives.GImage;
import forestmagicpp.preset.graphics.primitives.GRoundSquare;
import forestmagicpp.preset.graphics.primitives.GText;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.IOException;

public class GTicketIndicator extends GContainer {
    private final PathType pathType;
    private GText label;
    private GRoundSquare indicator;
    private boolean interactable = false;
    private int defaultLevel = 1;

    public GTicketIndicator(final PathType pathType, final TicketClickHandler ticketClickHandler) {
        this.pathType = pathType;

        indicator = new GRoundSquare();
        indicator.setFillPaint(getPaintForType(pathType, 1));
        indicator.setOutlinePaint(Color.BLACK);
        indicator.setStroke(new BasicStroke(0.01f));
        add(indicator);

        try {
            GImage image = getImageForType(pathType);
            image.scale(0.7, 0.7);
            add(image);
        } catch (IOException e) {
            System.err.println("Cannot load indicator image");
        }

        label = new GText();
        label.setTextPaint(GameGraphicConstants.LABEL_COLOR);
        label.scale(0.015, 0.015);
        label.translate(0.33, -0.35);
        label.setStroke(new BasicStroke(0.1f));
        label.setAutoScale(false);
        add(label);
        scale(0.8, 0.8);

        setOnMouseEnterCallback(() -> {
            if (interactable) {
                indicator.setFillPaint(getPaintForType(pathType, 2));
            }
        });
        setOnMouseLeaveCallback(() -> {
            indicator.setFillPaint(getPaintForType(pathType, defaultLevel));
        });
        setOnMouseClickCallback(() -> {
            if (interactable) {
                ticketClickHandler.handle(pathType);
            }
        });
    }

    public void setAmount(final int amount) {
        if (amount <= 0) {
            this.label.setText("0");
        } else if (amount < Integer.MAX_VALUE) {
            this.label.setText("" + amount);
        } else {
            this.label.setText("");
        }
    }

    public void setDefaultLevel(final int defaultLevel) {
        this.defaultLevel = defaultLevel;
        this.indicator.setFillPaint(getPaintForType(pathType, defaultLevel));
    }

    private GImage getImageForType(PathType type) throws IOException {
        String fileName = "/";
        switch (type) {
            case MOUSE_RIDE:
                fileName += "MOUSE.png";
                break;
            case CAT_RIDE:
                fileName += "CAT.png";
                break;
            case OWL_FLIGHT:
                fileName += "OWL.png";
                break;
            case SECRET_TUNNEL:
                fileName += "SECRET.png";
                break;
        }
        return new GImage(ImageIO.read(getClass().getResource(fileName)));
    }

    private Paint getPaintForType(PathType type, int level) {
        switch (level) {
            case 0:
                return GameGraphicConstants.PATH_INACTIVE_COLORS[type.ordinal()];
            case 1:
                return GameGraphicConstants.PATH_DEFAULT_COLORS[type.ordinal()];
            case 2:
                return GameGraphicConstants.PATH_HIGHLIGHT_COLORS[type.ordinal()];
            default:
                throw new IllegalArgumentException("No such level");
        }
    }

    public void setInteractable(final boolean value) {
        this.interactable = value;
    }
}
