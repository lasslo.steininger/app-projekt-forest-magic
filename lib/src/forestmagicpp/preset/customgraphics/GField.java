package forestmagicpp.preset.customgraphics;

import forestmagicpp.preset.PathType;
import forestmagicpp.preset.Position;
import forestmagicpp.preset.graphics.FieldClickHandler;
import forestmagicpp.preset.graphics.GContainer;
import forestmagicpp.preset.graphics.primitives.GCircle;

import java.awt.*;
import java.util.List;


public class GField extends GContainer {
    public static final double NORMAL_SCALE = 0.05;
    public static final double HOVER_SCALE = 0.1;
    public static final double SCALING_STEP = 0.5;

    private final double x, y;
    private final Position position;
    private GCircle hiddenIndicator;
    private GMultiField indicator;
    private boolean showBig = false;
    private boolean scale = false;
    private double size;
    private double scaleTarget;
    private boolean interactable = false;

    public GField(final double x, final double y, final Position position, final List<PathType> types,
                  final FieldClickHandler fieldClickHandler) {
        this.x = x;
        this.y = y;
        this.position = position;
        translate(x, y);
        scale(NORMAL_SCALE, NORMAL_SCALE);
        this.size = NORMAL_SCALE;
        if (types.contains(PathType.SECRET_TUNNEL)) {
            hiddenIndicator = new GCircle();
            hiddenIndicator.setFillPaint(Color.BLACK);
            //        hiddenIndicator.setVisible(hidden);
            add(hiddenIndicator);
        }
        indicator = new GMultiField(types);
        indicator.scale(0.8, 0.8);
        add(indicator);

        GCircle border = new GCircle();
        border.scale(0.8, 0.8);
        border.setOutlinePaint(Color.BLACK);
        border.setStroke(new BasicStroke(0.05f));
        add(border);

        setOnMouseClickCallback(() -> {
            if (interactable) {
                fieldClickHandler.handle(position);
            }
        });
        setOnMouseEnterCallback(() -> {
            if (interactable)
                scale(true);
        });
        setOnMouseLeaveCallback(() -> {
            scale(false);
        });
    }

    @Override
    public void update(float deltaTime) {
        if (!this.showBig && this.scale) {
            double direction = this.scaleTarget - this.size;
            double scaleStep;
            if (direction < 0) {
                scaleStep = Math.max(direction, -SCALING_STEP * deltaTime);
            } else {
                scaleStep = Math.min(direction, SCALING_STEP * deltaTime);
            }
            this.size += scaleStep;
            if (this.size == this.scaleTarget) {
                this.scale = false;
            }
            updateScale();
        }
    }

    public void showBig(boolean value) {
        this.showBig = value;
        this.scale = false;
        if (value) {
            this.size = HOVER_SCALE;
        } else {
            this.size = NORMAL_SCALE;
        }
        updateScale();
    }

    private void updateScale() {
        scale(this.size, this.size);
    }

    public void scale(boolean value) {
        this.scale = true;
        if (value) {
            this.scaleTarget = HOVER_SCALE;
        } else {
            this.scaleTarget = NORMAL_SCALE;
        }
    }

    public void setDefaultLevel(final int defaultLevel) {
        this.indicator.setDefaultLevel(defaultLevel);
    }

    public void setInteractable(final boolean value) {
        this.interactable = value;
        this.indicator.setInteractable(value);
    }

    public void highlightPath(PathType type) {
        this.indicator.highlightPath(type);
    }
}
