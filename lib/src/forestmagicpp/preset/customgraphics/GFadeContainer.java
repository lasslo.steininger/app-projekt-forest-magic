package forestmagicpp.preset.customgraphics;

import forestmagicpp.preset.graphics.GContainer;

public class GFadeContainer extends GContainer {
    private enum State {
        FADE_IN,
        FADE_OUT,
        IDLE
    }

    private double fadeSpeed = 1.0;
    private double visibility;
    private State state = State.IDLE;

    public GFadeContainer() {
        this(true);
    }

    public GFadeContainer(boolean initialVisible) {
        this.visibility = (initialVisible ? 1.0 : 0.0);
        alpha(visibility);
    }

    public void setFadeSpeed(double speed) {
        this.fadeSpeed = speed;
    }

    public void show() {
        this.state = State.FADE_IN;
    }

    public void hide() {
        this.state = State.FADE_OUT;
    }

    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);
        if (state == State.IDLE) {
            return;
        }
        if (state == State.FADE_IN) {
            visibility = Math.min(visibility + fadeSpeed * deltaTime, 1.0);
            if (visibility == 1.0) {
                state = State.IDLE;
            }
        } else {
            visibility = Math.max(visibility - fadeSpeed * deltaTime, 0.0);
            if (visibility == 0.0) {
                state = State.IDLE;
            }
        }
        alpha(visibility);
    }
}
