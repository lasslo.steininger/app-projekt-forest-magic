package forestmagicpp.preset.customgraphics;

import forestmagicpp.preset.PlayerRole;
import forestmagicpp.preset.Position;
import forestmagicpp.preset.graphics.primitives.GCircle;

import java.awt.*;
import java.awt.geom.Point2D;
import java.util.function.Function;

public class GPlayerToken extends GFadeContainer {
    private final Function<Position, Point2D> pointFunction;
    private final GCircle indicator;
    private Position position;
    private final Color normalColor;
    private final Color highlightColor;
    private static final double MOVE_SPEED = 0.025;
    private boolean moving = false;
    private Point2D targetPoint;
    private Position targetPosition;

    private boolean highlighted = false;
    private float time = 0f;

    public GPlayerToken(PlayerRole role, Function<Position, Point2D> pointFunction) {
        super(true);
        this.indicator = new GCircle();
        this.setFadeSpeed(3.0);

        if (role == PlayerRole.GOBLIN) {
            this.normalColor = GameGraphicConstants.GOBLIN_TOKEN_DEFAULT;
            this.highlightColor = GameGraphicConstants.GOBLIN_TOKEN_HIGHLIGHT;
        } else {
            this.normalColor = GameGraphicConstants.FAIRY_TOKEN_DEFAULT;
            this.highlightColor = GameGraphicConstants.FAIRY_TOKEN_HIGHLIGHT;
        }

        this.pointFunction = pointFunction;

        this.indicator.setFillPaint(this.normalColor);
        this.indicator.setOutlinePaint(Color.BLACK);
        this.indicator.setStroke(new BasicStroke(0.1f));
        add(indicator);

        scale(0.05, 0.05);
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(final Position position) {
        this.position = position;
        Point2D p = pointFunction.apply(position);
        translate(p.getX(), p.getY());
    }

    public void moveTo(final Position position) {
        targetPoint = pointFunction.apply(position);
        targetPosition = position;
        moving = true;
    }

    public void setHighlighted(boolean value) {
        if (!value) {
            this.indicator.setFillPaint(this.normalColor);
        }
        this.time = 0f;
        this.highlighted = value;
    }

    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);
        if (this.highlighted) {
            this.time += deltaTime;
            double value = 0.5 * Math.sin(this.time * 3.0) + 0.5;
            this.indicator.setFillPaint(fadeColor(this.normalColor, this.highlightColor, value));
        }
        if (moving) {
            double deltaX = targetPoint.getX() - getX();
            double deltaY = targetPoint.getY() - getY();
            Point2D delta = new Point2D.Double(deltaX, deltaY);
            double magnitude = magnitude(delta);
            if (magnitude > MOVE_SPEED) {
                delta = new Point2D.Double(deltaX / magnitude * MOVE_SPEED, deltaY / magnitude * MOVE_SPEED);
            }
            translate(getX() + delta.getX(), getY() + delta.getY());
            if (Math.abs(getX() - targetPoint.getX()) < 0.0001 && Math.abs(getY() - targetPoint.getY()) < 0.0001) {
                setPosition(targetPosition);
                moving = false;
            }
        }
    }

    private Color fadeColor(Color a, Color b, double value) {
        if (value < 0.0 || value > 1.0) {
            throw new IllegalArgumentException("value needs to be within 0.0 and 1.0");
        }
        int red = (int) (value * b.getRed() + (1 - value) * a.getRed());
        int green = (int) (value * b.getGreen() + (1 - value) * a.getGreen());
        int blue = (int) (value * b.getBlue() + (1 - value) * a.getBlue());
        return new Color(red, green, blue);
    }

    private double magnitude(Point2D p) {
        return Math.sqrt(p.getX() * p.getX() + p.getY() * p.getY());
    }
}
