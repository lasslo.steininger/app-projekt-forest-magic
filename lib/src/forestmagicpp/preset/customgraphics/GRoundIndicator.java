package forestmagicpp.preset.customgraphics;

import forestmagicpp.preset.PathType;
import forestmagicpp.preset.graphics.GContainer;
import forestmagicpp.preset.graphics.primitives.GCircle;
import forestmagicpp.preset.graphics.primitives.GText;

import java.awt.*;

public class GRoundIndicator extends GContainer {
    private GCircle indicatorCircle;
    private Paint fillPaint;
    private boolean highlighted = false;

    public GRoundIndicator(int round, int maxRounds, boolean visible) {
        indicatorCircle = new GCircle();
        indicatorCircle.translate(round - 1, 0.0);
        if (visible) {
            indicatorCircle.scale(0.7, 0.7);
            indicatorCircle.setStroke(new BasicStroke(0.15f));
        } else {
            indicatorCircle.scale(0.7, 0.7);
            indicatorCircle.setStroke(new BasicStroke(0.05f));
        }
        setActive(round == 1);
        fillPaint = GameGraphicConstants.BASE_COLOR;
        indicatorCircle.setFillPaint(fillPaint);
        GText indicatorLabel = new GText();
        indicatorLabel.setText("" + round);
        indicatorLabel.setAutoScale(false);
        indicatorLabel.setTextPaint(GameGraphicConstants.LABEL_COLOR);
        indicatorLabel.translate(round - 1, 0.0);
        indicatorLabel.scale(0.03, 0.03);
        add(indicatorCircle);
        add(indicatorLabel);
    }

    public void setUsedPath(PathType type) {
        switch (type) {
            case MOUSE_RIDE:
                fillPaint = GameGraphicConstants.MOUSE_RIDE_COLOR_DEFAULT;
                break;
            case CAT_RIDE:
                fillPaint = GameGraphicConstants.CAT_RIDE_COLOR_DEFAULT;
                break;
            case OWL_FLIGHT:
                fillPaint = GameGraphicConstants.OWL_FLIGHT_COLOR_DEFAULT;
                break;
            case SECRET_TUNNEL:
                fillPaint = GameGraphicConstants.SECRET_TUNNEL_COLOR_DEFAULT;
                break;
        }

        indicatorCircle.setFillPaint(fillPaint);
    }

    public void setActive(boolean value) {
        indicatorCircle.setOutlinePaint(
                value ? GameGraphicConstants.ROUND_INDICATOR_ACTIVE : GameGraphicConstants.ROUND_INDICATOR_INACTIVE);
    }

    public void setHighlighted(boolean value) {
        highlighted = value;
        // TODO ANIMATE
        if (highlighted) {
            Color c = (Color) fillPaint;
            indicatorCircle.setFillPaint(c.brighter());
        } else {
            indicatorCircle.setFillPaint(fillPaint);
        }
    }
}
