package forestmagicpp.preset.customgraphics;

import forestmagicpp.preset.PathType;
import forestmagicpp.preset.Position;

import java.util.Objects;

public class Path implements Comparable<Path> {
    private Position a;
    private Position b;
    private PathType type;

    public Path(final Position a, final Position b, final PathType type) {
        if (a.compareTo(b) < 0) {
            this.a = a;
            this.b = b;
        } else {
            this.a = b;
            this.b = a;
        }
        this.type = type;
    }

    public Position getA() {
        return a;
    }


    public Position getB() {
        return b;
    }

    public PathType getType() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Path path = (Path) o;
        return Objects.equals(a, path.a) && Objects.equals(b, path.b) && Objects.equals(type, path.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(a, b);
    }

    @Override
    public int compareTo(Path o) {
        return type.ordinal() - o.type.ordinal();
    }
}
