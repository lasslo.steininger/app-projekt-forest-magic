package forestmagicpp.preset;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.MissingArgumentException;
import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Der ArgumentParser wird verwendet, um Kommandozeilenparameter zu verarbeiten. Wenn Sie weitere
 * Kommandozeilenparameter hinzufuegen moechten, erweitern Sie diese Klasse und ueberschreiben die Methode {@link
 * #addMoreOptions(Options)}. Des Weiteren sollten dann weitere Methoden zum Abfragen der Werte der neuen Parameter
 * hinzugefuegt werden. Sie koennen sich dafuer an den bereits vorhandenen Methoden (z. B. {@link #getDelay()} oder
 * {@link #getPlayerNames()}) orientieren. Der genaue Umgang mit der hier verwendeten Apache Commons CLI wird auf deren
 * Website naeher spezifiziert. Ziehen Sie diese ggf. zu Rate.
 */
public class ArgumentParser {
    private CommandLine line;
    private Options options;

    public ArgumentParser(String[] args) throws ParseException {
        createOptions();
        initParser(args);
        parse();
    }

    protected final CommandLine getLine() {
        return line;
    }

    private final void createOptions() {
        options = new Options();

        Option help = Option.builder()
                            .longOpt("help")
                            .desc("print this message")
                            .required(false)
                            .hasArg(false)
                            .build();
        options.addOption(help);

        Option config = Option.builder()
                              .longOpt("config")
                              .desc("game configuration file")
                              .required(true)
                              .hasArg()
                              .argName("GAME CONFIGURATION FILE")
                              .optionalArg(false)
                              .type(File.class)
                              .build();
        options.addOption(config);

        Option playerTypes = Option.builder()
                                   .longOpt("playerTypes")
                                   .desc("player types [HUMAN, RANDOM_AI, SIMPLE_AI, ...]")
                                   .required(true)
                                   .hasArgs()
                                   .type(PlayerType.class)
                                   .build();
        options.addOption(playerTypes);

        Option playerNames = Option.builder()
                                   .longOpt("playerNames")
                                   .desc("player names")
                                   .required(true)
                                   .hasArgs()
                                   .type(String.class)
                                   .build();
        options.addOption(playerNames);

        Option delay = Option.builder()
                             .longOpt("delay")
                             .desc("delay in milliseconds between two moves")
                             .required(false)
                             .hasArg()
                             .argName("DELAY")
                             .type(Long.class)
                             .build();
        options.addOption(delay);

        addMoreOptions(options);
    }

    /**
     * Wenn Sie weitere Kommandozeilenparameter hinzufuegen moechten, muss diese Methode ueberschrieben werden. Dem
     * uebergebenen {@link Options}-Objekt muessen dann weitere {@link Option}s hinzugefuegt werden. Sie koennen sich
     * hierfuer an der Methode {@link #createOptions()} orientieren.
     *
     * @param options
     *         {@link Options}-Objekt, welches alle Kommandozeilen-Optionen enthaelt
     */
    public void addMoreOptions(Options options) {
        // Override this method to add more options.addOption() statements here..
    }

    private final void initParser(String[] args) throws ParseException {
        CommandLineParser parser = new DefaultParser();
        try {
            line = parser.parse(options, args);
        } catch (MissingArgumentException | MissingOptionException e) {
            System.err.println(e.getMessage());
            showHelp();
            throw e;
        }
    }

    private final void parse() {
        if (getLine() == null) {
            return;
        }

        if (getLine().hasOption("help")) {
            showHelp();
            System.exit(0);
        }
    }

    public final void showHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("java forestmagicpp.<YOUR-PACKAGE-OF-MAIN-CLASS>.<MAIN-CLASS>", options);
    }

    /**
     * Liest die mit dem Parameter {@code config} uebergebene Spielkonfigurationsdatei ein und liefert einen String mit
     * dem gesamten Dateiinhalt zurueck. Dieser String wird direkt an den Konstruktur von {@link GameConfiguration}
     * uebergeben.
     *
     * @return Dateiinhalt der Spielkonfiguration als String
     *
     * @throws IOException
     *         Tritt auf, falls die Spielkonfigurationsdatei nicht gelesen werden kann
     */
    public final String getGameConfiguration() throws IOException {
        if (getLine() == null || !getLine().hasOption("config")) {
            throw new IllegalStateException("This argument parser cannot be queried due to error in parsing phase!");
        }

        File configFile = new File(getLine().getOptionValue("config"));
        StringBuilder sb = new StringBuilder();
        InputStream is = new FileInputStream(configFile);
        BufferedReader br = new BufferedReader(new InputStreamReader(is));

        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line)
              .append("\n");
        }

        br.close();
        return sb.toString();
    }

    public final List<PlayerType> getPlayerTypes() {
        if (getLine() == null || !getLine().hasOption("playerTypes")) {
            throw new IllegalStateException("This argument parser cannot be queried due to error in parsing phase!");
        }

        String[] rawTypes = getLine().getOptionValues("playerTypes");
        List<PlayerType> playerTypes = new ArrayList<>(rawTypes.length);
        for (String s : rawTypes) {
            playerTypes.add(PlayerType.valueOf(s));
        }
        return playerTypes;
    }

    public final List<String> getPlayerNames() {
        if (getLine() == null || !getLine().hasOption("playerNames")) {
            throw new IllegalStateException("This argument parser cannot be queried due to error in parsing phase!");
        }

        String[] rawNames = getLine().getOptionValues("playerNames");
        List<String> playerNames = Arrays.asList(rawNames);
        return playerNames;
    }

    public final Long getDelay() {
        if (getLine() == null) {
            throw new IllegalStateException("This argument parser cannot be queried due to error in parsing phase!");
        }

        if (!getLine().hasOption("delay")) {
            return 500L;
        } else {
            return Long.parseLong(getLine().getOptionValue("delay"));
        }
    }
}
