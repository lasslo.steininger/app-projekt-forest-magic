package forestmagicpp.preset.move;

import java.io.Serializable;

/**
 * Die Klasse PlayerMove ist ein Wrapper um einen normalen {@link Move}. Er erweitert den Move um einen zusaetzlichen
 * Hash-Wert. Die Spielerklassen sollen ausschliesslich Objekte der Klassen {@link UnHashedMove} und {@link HashedMove}
 * erzeugen, um PlayerMoves ohne und mit Hashwert zu erzeugen. Beide dieser Klassen implementieren dieses Interface.
 */
public interface PlayerMove extends Serializable {
    /**
     * Gibt den gewrappten {@link Move} zurueck.
     *
     * @return Move
     */
    Move getMove();

    /**
     * Zeigt an, ob es einen Hashwert gibt.
     *
     * @return Ist true, wenn es einen Hashwert gibt
     */
    boolean hasHash();

    /**
     * Liefert den Hashwert zurueck.
     *
     * @return Der Hashwert als String
     */
    String getHash();
}
