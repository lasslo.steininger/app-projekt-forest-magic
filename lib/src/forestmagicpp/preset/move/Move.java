package forestmagicpp.preset.move;

import forestmagicpp.preset.PathType;
import forestmagicpp.preset.Position;

import java.io.Serializable;

public class Move implements Serializable {
    public static final Move PASS = new Move();

    private Position source;
    private Position destination;
    private PathType path;
    private boolean pass;

    private Move() {
        this.source = Position.UNKNOWN;
        this.destination = Position.UNKNOWN;
        this.path = PathType.MOUSE_RIDE;
        this.pass = true;
    }

    public Move(final Position source, final Position destination, final PathType path) {
        setSource(source);
        setDestination(destination);
        setPath(path);
        this.pass = false;
    }

    public Position getSource() {
        return source;
    }

    private void setSource(final Position source) {
        if (source == null) {
            throw new IllegalArgumentException("source == null");
        }
        this.source = source;
    }

    public Position getDestination() {
        return destination;
    }

    private void setDestination(final Position destination) {
        if (destination == null) {
            throw new IllegalArgumentException("destination == null");
        }
        this.destination = destination;
    }

    public PathType getPath() {
        return path;
    }

    private void setPath(final PathType path) {
        if (path == null) {
            throw new IllegalArgumentException("path == null");
        }
        this.path = path;
    }

    @Override
    public int hashCode() {
        return getSource().hashCode() ^ getDestination().hashCode();
    }

    @Override
    public boolean equals(final Object o) {
        if (o == null)
            return false;
        if (!(o instanceof Move))
            return false;
        Move m = (Move) o;
        return getSource().equals(m.getSource()) && getDestination().equals(m.getDestination()) &&
               getPath().equals(m.getPath()) && this.pass == m.pass;
    }

    @Override
    public String toString() {
        if (this.pass) {
            return "PASS";
        }
        return getSource() + " -> " + getDestination() + " [" + getPath() + "]";
    }
}
