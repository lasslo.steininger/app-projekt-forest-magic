package forestmagicpp.preset;

import forestmagicpp.preset.move.Move;

import java.util.Set;

/**
 * Die Methoden des {@link Viewer}s werden vom Spielfenster aufgerufen, um den aktuellen Spielstand anzeigen zu koennen.
 * Einige dieser Methoden koennen hauefiger aufgerufen werden, ohne dass sich der Spielstand waehrenddessen geaendert
 * hat. Stellen Sie bei der Implementierung von Ihnen sicher, dass wiederholte Aufrufe der Viewer-Methoden keine
 * unnoetige Laufzeit zur Folge haben. Sie duerfen an den Preset-Klassen nichts aendern. Wenn das wiederholte Aufrufen
 * dieser Methoden zur Folge hat, dass Ihr Programm stark verlangsamt wird, dann muessen Sie dieses Problem in Ihrer
 * Implementation anders loesen.
 */
public interface Viewer {
    /**
     * Liefert die Anzahl an Feenspielern zurueck. Da es immer exakt einen Goblin geben muss, entspricht diese Zahl der
     * Spieleranzahl - 1.
     *
     * @return Anzahl der Feen
     */
    int getNumberOfFairies();

    /**
     * Gibt die Spielernummer des Spieler zurueck, der gerade an der Reihe ist. Das ist der gleiche Wert, den jeder
     * Spieler bei seiner Initialisierung erhaelt. Wenn also der Goblin gerade an der Reihe ist, liefert diese Methode
     * den Wert {@code 0}.
     *
     * @return Zugnummer des aktuellen Spielers
     */
    int getActivePlayerNumber();

    /**
     * Gibt fuer den aktuellen Spieler die Anzahl seiner Koeder fuer einen bestimmten Pfadtypen zurueck. Im Falle vom
     * Pfad {@link PathType#SECRET_TUNNEL} entspricht der Wert keinem Koeder sondern der Anzahl verbleibender Nutzungen.
     * Der Goblin kann die alle Pfade bis auf den {@link PathType#SECRET_TUNNEL} {@link Integer#MAX_VALUE} oft benutzen.
     * Fuer alle anderen Faelle liefert diese Methode eine Zahl groesser oder gleich {@code 0} zurueck.
     *
     * @param type
     *         Pfadtyp
     *
     * @return Anzahl der Koeder des aktiven Spielers fuer diesen Pfad
     *
     * @throws IllegalArgumentException
     *         Falls fuer den uebergebenen Pfadtypen keine Koeder definiert sind (das sollte nicht auftreten, da das
     *         Enum nicht geandert werden darf!)
     */
    int getNumberOfAvailableLinkTypeUsages(PathType type) throws IllegalArgumentException;

    /**
     * Liefert die Position des Spielers mit uebergebener Spielernummer zurueck. Wenn der Goblin gerade nicht offen
     * spielt, wuerde der Aufruf fuer die {@code playerNumber 0} den Wert {@link Position#UNKNOWN} liefern.
     *
     * @param playerNumber
     *         Spielernummer
     *
     * @return Position des Spielers mit dieser Spielernummer
     *
     * @throws IllegalArgumentException
     *         Falls kein Spieler mit der uebergebenen Spielernummer existiert
     */
    Position getPositionOfPlayer(int playerNumber) throws IllegalArgumentException;

    /**
     * Liefert die Menge aller Positionen zurueck, die zum Wald gehoeren.
     *
     * @return Menge aller Waldpositionen
     */
    Set<Position> getBoardPositions();

    /**
     * Liefert die Menge aller Pfadtypen, die an der uebergebenen Position starten. Gehen von einem Feld also
     * beispielsweise sowohl Maeuse- als auch Katzenpfade aus, wuerde diese Methode fuer gesagtes Feld die Menge mit den
     * Elementen {@link PathType#MOUSE_RIDE} und {@link PathType#CAT_RIDE} enthalten.
     *
     * @param position
     *         Position
     *
     * @return Menge aller Pfadtypen, von denen Pfade diesen Typs von dieser Position ausgehen
     *
     * @throws IllegalArgumentException
     *         Falls diese Position nicht existiert
     */
    Set<PathType> getPositionTypes(Position position) throws IllegalArgumentException;

    /**
     * Liefert die Menge alle anderer Felder (durch Positionen), mit denen das uebergebene Feld durch einen
     * spezifizierten Pfadtyp verbunden ist. Ist also Feld {@code A} durch Maeusepfade mit den Felder {@code B} und
     * {@code C} verbunden, dann liefert der Aufruf von {@code getPositionLinks(A, PathType.MOUSE_RIDE)} eine Menge mit
     * den Positionen {@code B} und {@code C}.
     *
     * @param position
     *         Position von der ausgegangen wird
     * @param type
     *         Spezifizierter Pfadtyp
     *
     * @return Menge aller Positionen, die ueber den spezifizierten Pfadtypen vom ausgehenden Feld aus verbunden sind
     *
     * @throws IllegalArgumentException
     *         Falls diese Position oder dieser Pfadtyp nicht existieren
     */
    Set<Position> getPositionLinks(Position position, PathType type) throws IllegalArgumentException;

    /**
     * Liefert fuer den aktuellen Spieler die Menge aller moeglichen gueltigen Zuege zurueck. Wenn ein Spieler keinen
     * gueltigen Zug mehr ausfuehren kann, enthaelt die Menge den einzigen Move {@link Move#PASS}. Nur in diesem Fall
     * darf dieser Zug in der Menge auftauchen, da ein passen ansonsten nicht regelkonform ist.
     *
     * @return Die Menge aller gueltigen Zuege des aktuellen Spielers
     */
    Set<Move> getPossibleMoves();

    /**
     * Liefert den verwendeten Pfadtypen des Goblins einer bestimmten Runde zurueck. Diese Methode kommt beim Anzeigen
     * der Rundenindikatoren zum Einsatz. Wird der Pfadtyp einer Runde erfragt, in der der Goblin noch keinen Pfadtypen
     * gewaehlt hat, reagiert diese Methode darauf mit einer {@link IllegalArgumentException}.
     *
     * @param round
     *         Runde
     *
     * @return Verwendeter Pfadtyp des Goblins in der uebergebenen Runde
     *
     * @throws IllegalArgumentException
     *         Falls diese Runde nicht existiert oder der Goblin in dieser Runde noch keinen Pfad gewaehlt hat
     */
    PathType getGoblinMoveTypeFromRound(int round) throws IllegalArgumentException;

    /**
     * Liefert die aktuelle Rundennummer zurueck. Der erste Zug des Spiels und damit vom Goblin gehoert zur 1. Runde.
     *
     * @return Aktuelle Spielrunde
     */
    int getRound();

    /**
     * Gibt die Menge aller offenen Runden zurueck. Diese Menge wird in der Spielkonfiguration festgelegt.
     *
     * @return Menge aller offenen Runden.
     */
    Set<Integer> getVisibleRounds();

    /**
     * Gibt die maximale Rundenzahl zurueck, bis das Spiel endet. Diese Anzahl wird in der Spielkonfiguration
     * festgelegt.
     *
     * @return Maximale Rundenzahl
     */
    int getMaxNumberOfRounds();
}
