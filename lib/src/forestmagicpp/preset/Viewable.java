package forestmagicpp.preset;

public interface Viewable {
    Viewer viewer();
}
