package forestmagicpp.preset.graphics;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;

public abstract class GObject implements Drawable, Updateable {
    private boolean enabled = true;
    private boolean visible = true;
    private AffineTransform transform;
    private AffineTransform inverseGraphicsTransform;
    private boolean mouseOver = false;
    private double x = 0.0;
    private double y = 0.0;
    private double scaleX = 1.0;
    private double scaleY = 1.0;
    private double shearX = 0.0;
    private double shearY = 0.0;
    private double rot = 0.0;
    private Composite composite;
    private double alpha = 1.0;

    private Callback onMouseEnterCallback;
    private Callback onMouseLeaveCallback;
    private Callback onMouseClickCallback;
    private Callback onMousePressCallback;
    private Callback onMouseReleaseCallback;

    public GObject() {
        recalculateTransform();
        alpha(1.0);
    }

    @Override
    public synchronized void draw(final Graphics2D g) {
        if (!enabled || !visible) {
            return;
        }
        AffineTransform backupTransform = g.getTransform();
        Composite backupComposite = g.getComposite();
        g.transform(transform);
        try {
            this.inverseGraphicsTransform = g.getTransform()
                                             .createInverse();
        } catch (NoninvertibleTransformException e) {
            e.printStackTrace();
        }
        if (getAlpha() != 1.0) {
            g.setComposite(this.composite);
        }
        abstractDraw(g);
        g.setComposite(backupComposite);
        g.setTransform(backupTransform);
    }

    protected AffineTransform getTransform() {
        return this.transform;
    }

    protected AffineTransform getInverseGraphicsTransform() {
        return this.inverseGraphicsTransform;
    }

    public boolean isEnabled() {
        return this.enabled;
    }

    public synchronized void setEnabled(final boolean value) {
        this.enabled = value;
    }

    public boolean isVisible() {
        return this.visible;
    }

    public synchronized void setVisible(final boolean value) {
        this.visible = value;
    }

    protected abstract void abstractDraw(final Graphics2D g);

    public abstract boolean contains(final Point point);

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public Point2D getPoint() {
        return new Point2D.Double(getX(), getY());
    }

    public double getAlpha() {
        return this.alpha;
    }

    public synchronized void translate(final double x, final double y) {
        this.x = x;
        this.y = y;
        recalculateTransform();
    }

    public synchronized void scale(final double scaleX, final double scaleY) {
        this.scaleX = scaleX;
        this.scaleY = scaleY;
        recalculateTransform();
    }

    public synchronized void shear(final double shearX, final double shearY) {
        this.shearX = shearX;
        this.shearY = shearY;
        recalculateTransform();
    }

    public synchronized void rotate(final double rot) {
        this.rot = rot;
        recalculateTransform();
    }

    public synchronized void alpha(final double alpha) {
        this.alpha = alpha;
        composite = createComposite(this.alpha);
    }

    private synchronized AlphaComposite createComposite(double alpha) {
        return AlphaComposite.getInstance(AlphaComposite.SRC_OVER, (float) alpha);
    }

    private synchronized void recalculateTransform() {
        transform = new AffineTransform();
        transform.shear(shearX, shearY);
        transform.translate(x, y);
        transform.rotate(Math.toRadians(rot));
        transform.scale(scaleX, scaleY);
    }

    public void setOnMouseEnterCallback(Callback callback) {
        this.onMouseEnterCallback = callback;
    }

    public void setOnMouseLeaveCallback(Callback callback) {
        this.onMouseLeaveCallback = callback;
    }

    public void setOnMouseClickCallback(Callback callback) {
        this.onMouseClickCallback = callback;
    }

    public void setOnMousePressCallback(Callback callback) {
        this.onMousePressCallback = callback;
    }

    public void setOnMouseReleaseCallback(Callback callback) {
        this.onMouseReleaseCallback = callback;
    }


    synchronized void mouseOver(Point point, boolean value) {
        if (!enabled) {
            return;
        }
        if (mouseOver != value) {
            mouseOver = value;
            if (mouseOver) {
                onMouseEnter();
            } else {
                onMouseLeave();
            }
        }
    }

    private synchronized void onMouseEnter() {
        if (!enabled) {
            return;
        }
        if (onMouseEnterCallback != null) {
            onMouseEnterCallback.call();
        }
    }

    private synchronized void onMouseLeave() {
        if (!enabled) {
            return;
        }
        if (onMouseLeaveCallback != null) {
            onMouseLeaveCallback.call();
        }
    }

    synchronized void onMouseClick(Point point) {
        if (!enabled) {
            return;
        }
        if (onMouseClickCallback != null) {
            onMouseClickCallback.call();
        }
    }

    synchronized void onMousePress(Point point) {
        if (!enabled) {
            return;
        }
        if (onMousePressCallback != null) {
            onMousePressCallback.call();
        }
    }

    synchronized void onMouseRelease(Point point) {
        if (!enabled) {
            return;
        }
        if (onMouseReleaseCallback != null) {
            onMouseReleaseCallback.call();
        }
    }
}
