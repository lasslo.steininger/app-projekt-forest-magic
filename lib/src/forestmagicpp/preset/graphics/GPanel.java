package forestmagicpp.preset.graphics;

import forestmagicpp.preset.graphics.input.MouseInfo;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class GPanel extends JPanel implements Updateable {

    private List<GObject> gObjects = new ArrayList<>();
    private double scaleX = 1.0;
    private double scaleY = -1.0;

    private Object VALUE_ANTIALIAS;
    private Object VALUE_TEXT_ANTIALIAS;
    private Object VALUE_RENDERING;
    private Object VALUE_ALPHA_INTERPOLATION;
    private Object VALUE_COLOR_RENDERING;
    private Object VALUE_DITHERING;
    private Object VALUE_FRACTIONALMETRICS;
    private Object VALUE_INTERPOLATION;
    private Object VALUE_STROKE_CONTROL;

    GPanel() {
        this.setDoubleBuffered(true);
        setQuality(GQuality.HIGH);
    }

    void setQuality(GQuality quality) {
        switch (quality) {
            case LOW:
                VALUE_ANTIALIAS = RenderingHints.VALUE_ANTIALIAS_OFF;
                VALUE_TEXT_ANTIALIAS = RenderingHints.VALUE_TEXT_ANTIALIAS_OFF;
                VALUE_RENDERING = RenderingHints.VALUE_RENDER_SPEED;
                VALUE_ALPHA_INTERPOLATION = RenderingHints.VALUE_ALPHA_INTERPOLATION_SPEED;
                VALUE_COLOR_RENDERING = RenderingHints.VALUE_COLOR_RENDER_SPEED;
                VALUE_DITHERING = RenderingHints.VALUE_DITHER_DISABLE;
                VALUE_FRACTIONALMETRICS = RenderingHints.VALUE_FRACTIONALMETRICS_OFF;
                VALUE_INTERPOLATION = RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR;
                VALUE_STROKE_CONTROL = RenderingHints.VALUE_STROKE_PURE;
                break;
            case HIGH:
                VALUE_ANTIALIAS = RenderingHints.VALUE_ANTIALIAS_ON;
                VALUE_TEXT_ANTIALIAS = RenderingHints.VALUE_TEXT_ANTIALIAS_ON;
                VALUE_RENDERING = RenderingHints.VALUE_RENDER_QUALITY;
                VALUE_ALPHA_INTERPOLATION = RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY;
                VALUE_COLOR_RENDERING = RenderingHints.VALUE_COLOR_RENDER_QUALITY;
                VALUE_DITHERING = RenderingHints.VALUE_DITHER_ENABLE;
                VALUE_FRACTIONALMETRICS = RenderingHints.VALUE_FRACTIONALMETRICS_ON;
                VALUE_INTERPOLATION = RenderingHints.VALUE_INTERPOLATION_BICUBIC;
                VALUE_STROKE_CONTROL = RenderingHints.VALUE_STROKE_NORMALIZE;
                break;
            default:
                throw new IllegalArgumentException("No such quality setting: " + quality);
        }
    }

    @Override
    protected void paintComponent(final Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        setupGraphics2D(g2d);
        adjust(g2d);
        drawGraphicsObjects(g2d);
    }

    private void setupGraphics2D(final Graphics2D g) {
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, VALUE_ANTIALIAS);
        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, VALUE_TEXT_ANTIALIAS);
        g.setRenderingHint(RenderingHints.KEY_RENDERING, VALUE_RENDERING);
        g.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, VALUE_ALPHA_INTERPOLATION);
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, getWidth(), getHeight());
    }

    private void adjust(final Graphics2D g2d) {
        double width = getWidth();
        double height = getHeight();
        g2d.translate(width / 2, height / 2);
        //        g2d.scale(scaleX / width, scaleY / height);
        g2d.scale(scaleX * width / 2, scaleY * height / 2);
    }

    private synchronized void drawGraphicsObjects(final Graphics2D g) {
        for (GObject go : gObjects) {
            go.draw(g);
        }
    }

    public void scale(final double scaleX, final double scaleY) {
        this.scaleX = 1.0 * scaleX;
        this.scaleY = -1.0 * scaleY;
    }

    public synchronized void add(final GObject gObject) {
        gObjects.add(gObject);
    }

    public synchronized void remove(final GObject gObject) {
        gObjects.remove(gObject);
    }

    public synchronized void clear() {
        gObjects.clear();
    }

    public synchronized void handleMouseInput(final MouseInfo info) {
        for (GObject go : gObjects) {
            switch (info.getState()) {
                case CLICKED:
                    if (go.contains(info.getPoint())) {
                        go.onMouseClick(info.getPoint());
                    }
                    break;
                case MOVED:
                    go.mouseOver(info.getPoint(), go.contains(info.getPoint()));
                    break;
                case PRESSED:
                    if (go.contains(info.getPoint())) {
                        go.onMousePress(info.getPoint());
                    }
                    break;
                case RELEASED:
                    if (go.contains(info.getPoint())) {
                        go.onMouseRelease(info.getPoint());
                    }
                    break;
            }
        }
    }

    @Override
    public synchronized void update(float deltaTime) {
        for (GObject go : gObjects) {
            go.update(deltaTime);
        }
    }
}
