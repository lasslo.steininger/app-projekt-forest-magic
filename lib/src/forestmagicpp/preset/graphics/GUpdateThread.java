package forestmagicpp.preset.graphics;

import forestmagicpp.preset.graphics.primitives.GText;

import java.util.ArrayDeque;
import java.util.Queue;

class GUpdateThread implements Runnable {
    public static final int NUMBER_OF_FRAMES_TO_CONSIDER_FOR_FPS = 100;

    private Thread thread;
    private GPanel panel;
    private long timeBetweenUpdates = 1000000000L / 60L;
    private long nextUpdateTime = 0L;
    private long lastUpdateTime;
    private long lastRenderTime = 0L;
    private boolean running = false;
    private boolean activeRendering = true;

    private Queue<Double> renderTimes = new ArrayDeque<>(NUMBER_OF_FRAMES_TO_CONSIDER_FOR_FPS);
    private double renderTimeSum = 0.0;
    private GText fpsText;

    public GUpdateThread(final GPanel panel, final GText fpsText) {
        this.panel = panel;
        this.fpsText = fpsText;
        thread = new Thread(this, "Render Thread");
        this.thread.start();
    }

    public void setUpdateRate(final int updatesPerSecond) {
        this.timeBetweenUpdates = 1000000000L / updatesPerSecond;
    }

    private long counter = 0L;
    @Override
    public void run() {
        while (!thread.isInterrupted()) {
            try {
                if (this.running) {
                    long currentTime = System.nanoTime();

                    if (currentTime > nextUpdateTime) {
                        nextUpdateTime = currentTime + timeBetweenUpdates;
                        panel.update((float) ((currentTime - lastUpdateTime) / 1000000000.0));
                        lastUpdateTime = currentTime;
                    }

                    long currentRenderTime = System.currentTimeMillis();
                    double deltaRenderTime = (currentRenderTime - lastRenderTime) / 1000.0;
                    lastRenderTime = currentRenderTime;

                    while (this.renderTimes.size() >= NUMBER_OF_FRAMES_TO_CONSIDER_FOR_FPS) {
                        this.renderTimeSum -= this.renderTimes.poll();
                    }
                    while (this.renderTimes.size() < NUMBER_OF_FRAMES_TO_CONSIDER_FOR_FPS) {
                        this.renderTimeSum += deltaRenderTime;
                        this.renderTimes.add(deltaRenderTime);
                    }

                    this.fpsText.setText("FPS: " + (long) getSmoothedFPS());
//                    panel.paintImmediately(0, 0, panel.getWidth(), panel.getHeight());
                    panel.repaint(0L);

                    if (!activeRendering) {
                        this.running = false;
                    }
                } else {
                    synchronized (this) {
                        wait();
                    }
                }
                //                                Thread.sleep(10);
            } catch (InterruptedException e) {
                // Nothing to do, just stop updating
            }
        }
    }

    public double getSmoothedFPS() {
        return NUMBER_OF_FRAMES_TO_CONSIDER_FOR_FPS / this.renderTimeSum;
    }

    public synchronized void start() {
        if (this.running) {
            return;
        }
        this.running = true;
        notify();
    }

    public void stop() {
        this.running = false;
    }

    public void kill() {
        thread.interrupt();
    }

    public void setActiveRendering(boolean value) {
        this.activeRendering = value;
    }
}
