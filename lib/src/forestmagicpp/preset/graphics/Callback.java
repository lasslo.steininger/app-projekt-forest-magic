package forestmagicpp.preset.graphics;

@FunctionalInterface
public interface Callback {
    void call();
}
