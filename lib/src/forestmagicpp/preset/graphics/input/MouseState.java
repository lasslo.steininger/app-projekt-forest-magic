package forestmagicpp.preset.graphics.input;

public enum MouseState {
    MOVED,
    CLICKED,
    PRESSED,
    RELEASED
}
