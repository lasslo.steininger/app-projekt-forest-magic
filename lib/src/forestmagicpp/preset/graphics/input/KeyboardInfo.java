package forestmagicpp.preset.graphics.input;

import java.awt.event.KeyEvent;

public class KeyboardInfo {
    private final int keyCode;
    private final KeyboardState state;

    public KeyboardInfo(final int keyCode, final KeyboardState state) {
        this.keyCode = keyCode;
        this.state = state;
    }

    public int getKeyCode() {
        return this.keyCode;
    }

    public String getKeyText() {
        return KeyEvent.getKeyText(getKeyCode());
    }

    public char getKeyChar() {
        return (char) getKeyCode();
    }

    public KeyboardState getState() {
        return this.state;
    }
}
