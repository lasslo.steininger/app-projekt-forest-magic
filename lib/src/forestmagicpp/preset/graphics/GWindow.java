package forestmagicpp.preset.graphics;

import forestmagicpp.preset.graphics.input.KeyboardInfo;
import forestmagicpp.preset.graphics.input.KeyboardState;
import forestmagicpp.preset.graphics.input.MouseButton;
import forestmagicpp.preset.graphics.input.MouseInfo;
import forestmagicpp.preset.graphics.input.MouseState;
import forestmagicpp.preset.graphics.primitives.GText;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.function.Consumer;

public class GWindow extends JFrame {
    private GPanel gPanel = new GPanel();
    private GUpdateThread gUpdateThread;
    private Callback onOpenCallback;
    private Callback onCloseCallback;
    private Callback onFocusGainedCallback;
    private Callback onFocusLostCallback;
    private Consumer<KeyboardInfo> keyboardConsumer;
    private Consumer<MouseInfo> mouseConsumer;

    private GText fpsText;

    public GWindow(final String title) {
        setTitle(title);

        add(gPanel);
        pack();

        setIgnoreRepaint(true);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

        gPanel.requestFocus();

        fpsText = new GText();
        fpsText.setAutoScale(false);
        fpsText.setAutoCenter(false);
        fpsText.setTextPaint(Color.WHITE);
        fpsText.scale(0.01, 0.01);
        fpsText.translate(0.4, 0.9);
        fpsText.setFont(new Font("MONOSPACED", Font.PLAIN, 10));

        gUpdateThread = new GUpdateThread(gPanel, fpsText);
        gUpdateThread.setUpdateRate(50);

        setupListeners();
    }

    public void setQuality(GQuality quality) {
        gPanel.setQuality(quality);
    }

    public void setSize(final int width, final int height) {
        Dimension size = new Dimension(width, height);
        gPanel.setPreferredSize(size);
        gPanel.setMinimumSize(size);
        pack();
    }

    private void setupListeners() {
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowOpened(WindowEvent e) {
                if (onOpenCallback != null) {
                    onOpenCallback.call();
                }
            }

            @Override
            public void windowClosing(WindowEvent e) {
                if (onCloseCallback != null) {
                    onCloseCallback.call();
                }
                gUpdateThread.kill();
                dispose();
            }

            @Override
            public void windowActivated(WindowEvent e) {
                if (onFocusGainedCallback != null) {
                    onFocusGainedCallback.call();
                }
            }

            @Override
            public void windowDeactivated(WindowEvent e) {
                if (onFocusLostCallback != null) {
                    onFocusLostCallback.call();
                }
            }
        });

        gPanel.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (keyboardConsumer != null) {
                    keyboardConsumer.accept(new KeyboardInfo(e.getKeyCode(), KeyboardState.PRESSED));
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
                if (keyboardConsumer != null) {
                    keyboardConsumer.accept(new KeyboardInfo(e.getKeyCode(), KeyboardState.RELEASED));
                }
            }
        });

        gPanel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                processMouse(e.getPoint(), convertToMouseButton(e.getButton()), MouseState.CLICKED);
            }

            @Override
            public void mousePressed(MouseEvent e) {
                processMouse(e.getPoint(), convertToMouseButton(e.getButton()), MouseState.PRESSED);
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                processMouse(e.getPoint(), convertToMouseButton(e.getButton()), MouseState.RELEASED);
            }

            @Override
            public void mouseMoved(MouseEvent e) {
                processMouse(e.getPoint(), convertToMouseButton(e.getButton()), MouseState.MOVED);
            }
        });
        gPanel.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                processMouse(e.getPoint(), convertToMouseButton(e.getButton()), MouseState.MOVED);
            }
        });
    }

    private void processMouse(final Point point, final MouseButton button, final MouseState state) {
        MouseInfo info = new MouseInfo(point, button, state);
        if (mouseConsumer != null) {
            mouseConsumer.accept(info);
        }
        gPanel.handleMouseInput(info);
    }

    public void setOnOpenCallback(Callback onOpenCallback) {
        this.onOpenCallback = onOpenCallback;
    }

    public void setOnCloseCallback(Callback onCloseCallback) {
        this.onCloseCallback = onCloseCallback;
    }

    public void setOnFocusGainedCallback(Callback onFocusGainedCallback) {
        this.onFocusGainedCallback = onFocusGainedCallback;
    }

    public void setOnFocusLostCallback(Callback onFocusLostCallback) {
        this.onFocusLostCallback = onFocusLostCallback;
    }

    public void setKeyboardCallback(Consumer<KeyboardInfo> callback) {
        this.keyboardConsumer = callback;
    }

    public void setMouseCallback(Consumer<MouseInfo> callback) {
        this.mouseConsumer = callback;
    }

    public void add(final GObject gObject) {
        gPanel.add(gObject);
    }

    public void remove(final GObject gObject) {
        gPanel.remove(gObject);
    }

    public void clear() {
        gPanel.clear();
    }

    public void startUpdating() {
        this.gUpdateThread.start();
    }

    public void stopUpdating() {
        this.gUpdateThread.stop();
    }

    public void setActiveRendering(boolean value) {
        this.gUpdateThread.setActiveRendering(value);
    }

    public void singleUpdate() {
        this.gUpdateThread.start();
    }

    public void setUpdateRate(int ups) {
        this.gUpdateThread.setUpdateRate(ups);
    }

    private MouseButton convertToMouseButton(int button) {
        switch (button) {
            case MouseEvent.BUTTON1:
                return MouseButton.PRIMARY;
            case MouseEvent.BUTTON2:
                return MouseButton.MIDDLE;
            case MouseEvent.BUTTON3:
                return MouseButton.SECONDARY;
            default:
                return MouseButton.NONE;
        }
    }

    public void showDebug(boolean value) {
        if (value) {
            add(fpsText);
        } else {
            remove(fpsText);
        }
    }
}
