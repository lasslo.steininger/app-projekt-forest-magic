package forestmagicpp.preset.graphics.primitives;

import java.awt.geom.Arc2D;

public class GArc extends GShape {
    public GArc(final double start, final double extend, final int type) {
        super(new Arc2D.Double(-0.5, -0.5, 1.0, 1.0, start, extend, type));
    }
}
