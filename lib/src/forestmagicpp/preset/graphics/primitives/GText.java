package forestmagicpp.preset.graphics.primitives;

import forestmagicpp.preset.graphics.GObject;

import java.awt.*;
import java.awt.font.GlyphVector;
import java.awt.geom.Rectangle2D;

public class GText extends GObject {
    private String text;
    private Font font;
    private Stroke stroke;
    private Paint textPaint;
    private Paint outlinePaint;
    private boolean autoScale = true;
    private boolean autoCenter = true;

    public GText() {
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Font getFont() {
        return font;
    }

    public void setFont(Font font) {
        this.font = font;
    }

    public Stroke getStroke() {
        return stroke;
    }

    public void setStroke(Stroke stroke) {
        this.stroke = stroke;
    }

    public Paint getTextPaint() {
        return textPaint;
    }

    public void setTextPaint(Paint textPaint) {
        this.textPaint = textPaint;
    }

    public Paint getOutlinePaint() {
        return outlinePaint;
    }

    public void setOutlinePaint(Paint outlinePaint) {
        this.outlinePaint = outlinePaint;
    }

    public boolean isAutoScale() {
        return autoScale;
    }

    public void setAutoScale(boolean autoScale) {
        this.autoScale = autoScale;
    }

    public boolean isAutoCenter() {
        return autoCenter;
    }

    public void setAutoCenter(boolean autoCenter) {
        this.autoCenter = autoCenter;
    }

    @Override
    protected void abstractDraw(Graphics2D g) {
        Font backupFont = null;
        if (this.font != null) {
            backupFont = g.getFont();
            g.setFont(this.font);
        }
        String text = getText();
        if (text != null) {
            g.scale(1.0, -1.0);
            FontMetrics metrics = g.getFontMetrics(g.getFont());

            Rectangle2D bounds = metrics.getStringBounds(text, g);
            double width = bounds.getWidth();
            if (autoScale) {
                g.scale(1.0 / width, 1.0 / width);
            }

            GlyphVector glyphVector = g.getFont()
                                       .createGlyphVector(g.getFontRenderContext(), text);
            Shape shape = glyphVector.getOutline();
            Rectangle2D shapeBounds = shape.getBounds2D();
            if (autoCenter) {
                g.translate(-shapeBounds.getWidth() / 2.0, shapeBounds.getHeight() / 2.0);
            }

            if (textPaint != null) {
                g.setPaint(textPaint);
                g.fill(shape);
            }
            if (outlinePaint != null) {
                if (stroke != null) {
                    g.setStroke(stroke);
                }
                g.setPaint(outlinePaint);
                g.draw(shape);
            }
        }

        if (backupFont != null) {
            g.setFont(backupFont);
        }
    }

    @Override
    public boolean contains(Point point) {
        return false;
    }

    @Override
    public void update(float deltaTime) {

    }
}
