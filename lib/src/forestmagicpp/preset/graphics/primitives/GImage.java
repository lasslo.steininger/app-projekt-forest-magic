package forestmagicpp.preset.graphics.primitives;

import forestmagicpp.preset.graphics.GObject;

import java.awt.*;

public class GImage extends GObject {
    private final Image image;
    private final double width;
    private final double height;

    public GImage(final Image image) {
        if (image == null) {
            throw new IllegalArgumentException("image cannot be null");
        }
        this.image = image;
        this.width = image.getWidth(null);
        this.height = image.getHeight(null);
    }

    @Override
    protected void abstractDraw(Graphics2D g) {
        g.scale(1.0 / this.width, -1.0 / this.height);
        g.translate(-this.width / 2, -this.height / 2);
        g.drawImage(this.image, null, null);
    }

    @Override
    public boolean contains(Point point) {
        // TODO
        return false;
    }

    @Override
    public void update(float deltaTime) {
        // Do nothing per default
    }
}
