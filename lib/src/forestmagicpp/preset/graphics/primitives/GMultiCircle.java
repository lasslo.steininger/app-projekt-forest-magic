package forestmagicpp.preset.graphics.primitives;

import forestmagicpp.preset.graphics.GContainer;

import java.awt.*;
import java.awt.geom.Arc2D;

public class GMultiCircle extends GContainer {
    private GArc[] arcs;

    public GMultiCircle(final Color[] colors) {
        arcs = new GArc[colors.length];

        double size = 360.0 / colors.length;
        for (int i = 0; i < colors.length; i++) {
            GArc arc = new GArc(90.0 + i * size, size, Arc2D.PIE);
            arc.setFillPaint(colors[i]);
            arcs[i] = arc;
            add(arc);
        }

        setOnMouseEnterCallback(() -> {
            for (int i = 0; i < arcs.length; i++) {
                arcs[i].setFillPaint(colors[i].brighter());
            }
        });
        setOnMouseLeaveCallback(() -> {
            for (int i = 0; i < arcs.length; i++) {
                arcs[i].setFillPaint(colors[i]);
            }
        });
    }
}
