package forestmagicpp.preset.graphics.primitives;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

public class GLine extends GShape {
    public GLine(Point2D p1, Point2D p2) {
        super(new Line2D.Double(p1, p2));
    }
}
