package forestmagicpp.preset.graphics.primitives;

import java.awt.geom.Ellipse2D;

public class GCircle extends GShape {
    public GCircle() {
        super(new Ellipse2D.Double(-0.5, -0.5, 1.0, 1.0));
    }
}
