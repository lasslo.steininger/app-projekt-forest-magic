package forestmagicpp.preset.graphics.primitives;

import java.awt.geom.RoundRectangle2D;

public class GRoundSquare extends GShape {
    public GRoundSquare(double arcw, double arch) {
        super(new RoundRectangle2D.Double(-0.5, -0.5, 1.0, 1.0, arcw, arch));
    }

    public GRoundSquare() {
        super(new RoundRectangle2D.Double(-0.5, -0.5, 1.0, 1.0, 0.1, 0.1));
    }
}
