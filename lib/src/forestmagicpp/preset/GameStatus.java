package forestmagicpp.preset;

public enum GameStatus {
    FAIRIES_WIN,
    GOBLIN_WINS,
    ILLEGAL
}
