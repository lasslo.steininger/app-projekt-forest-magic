package forestmagicpp.preset;

import forestmagicpp.preset.move.Move;
import forestmagicpp.preset.move.PlayerMove;

import java.rmi.Remote;
import java.util.List;

/**
 * Das Spieler Interface wird verwendet, um mit einem Spieler zu kommunizieren. Alle Spieler muessen dieses Interface
 * implementieren. Viele Methoden dieses Interfaces sehen vor, dass eine {@link Exception} geworfen werden kann. Eine
 * solche Exception muessen Sie nicht werfen. Dieses Konzept ist notwendig fuer ein Netzwerkspiel, dass
 * situationsbedingt in dem aktuellen Semester nicht mehr Teilanforderung an das Projekt ist. Dennoch bleibt diesen
 * diese Exceptions im Player Interface, um diesen in Zukunft ggf. um eine Netzwerkanbindung zu erweitern.
 */
public interface Player extends Remote {
    /**
     * Liefert den Namen des Spielers zurueck. Jeder Spieler bekommt seinen Namen bei Programmstart auf der
     * Kommandozeile uebergeben.
     *
     * @return Name des Spielers
     *
     * @throws Exception
     *         Falls es Netzwerkprobleme gibt oder der Spieler keinen Namen hat
     */
    String name() throws Exception;

    /**
     * Initialisiert den Spieler und teilt ihm die verwendete Spielkonfiguration, seine Rolle und seine Zugreihenfolge
     * mit. Die Zugreihenfolge bestimmt (0-indiziert), wann der Spieler in einer Runde am Zug ist. Der Goblin hat fuer
     * {@code turn} immer den Wert {@code 0}.
     *
     * @param gameConfiguration
     *         Verwendete Spielkonfiguration
     * @param role
     *         Rolle des Spielers
     * @param turn
     *         Zugreihenfolge
     *
     * @throws Exception
     *         Falls es Netzwerkprobleme gibt oder das Initialisieren fehlschlaegt
     */
    void init(GameConfiguration gameConfiguration, PlayerRole role, int turn) throws Exception;

    /**
     * Fordert vom Spieler einen Zug an.
     *
     * @return Zug als Objekt einer Klasse, die {@link PlayerMove} implementiert
     *
     * @throws Exception
     *         Falls es Netzwerkprobleme gibt oder das Anfordern des Zuges einen Fehler verursacht
     */
    PlayerMove requestMove() throws Exception;

    /**
     * Informiert den Spieler ueber den Zug eines anderen Spielers, der an der Reihe ist.
     *
     * @param otherMove
     *         Zug des anderen Spielers als Objekt einer Klasse, die {@link PlayerMove} implementiert.
     *
     * @throws Exception
     *         Falls es Netzwerkprobleme gibt oder das Benachrichtigen des Spielers fehlschlaegt
     */
    void notifyMove(PlayerMove otherMove) throws Exception;

    /**
     * Erfragt vom Spieler den Status des zuletzt ausgefuehrten Zuges. Das kann ein Zug sein, den der Spieler selbst
     * ueber {@link #requestMove()} gewaehlt hat oder ein Zug eines anderen Spielers, der ihm mit {@link
     * #notifyMove(PlayerMove)} uebergeben wurde.
     *
     * @return Status des zuletzt ausgefuehrten Zuges
     *
     * @throws Exception
     *         Falls es Netzwerkprobleme gibt oder das Status erfragen fehlschlaegt
     */
    MoveStatus requestStatus() throws Exception;

    /**
     * Teilt dem Spieler den Gesamtstatus mit, den der letzte Zug im Gesamtspiel zur Folge hatte. Dieser kann sich vom
     * Status, den der Spieler fuer sich erkannt hat unterscheiden. Naehere Informationen sind der
     * Implementierungsbeschreibung zu entnehmen.
     *
     * @param status
     *         Gesamtstatus des letzten Zuges
     *
     * @throws Exception
     *         Falls es Netzwerkprobleme gibt oder das Status benachrichtigen fehlschlaegt
     */
    void notifyStatus(MoveStatus status) throws Exception;

    /**
     * Erfragt vom Spieler alle seine im Spiel bisher ausgefuehrten Zuege. Hier sind explizit nur die Zuginformationen
     * gefragt ohne Hashwerte. Nur der Goblin muss auf diesen Aufruf reagieren. Wird die Methode von Feen aufgerufen,
     * ist das ein Fehler. Die Hashwerte sind hier nicht erforderlich, da die Feen diese ohnehin selbst berechnen
     * muessen, um die Zuege des Goblins verifizieren zu koennen. Die in dieser Liste enthaltenen Zuege muessen alle
     * offen sein und die Positionen des Goblins in jeder Runde preisgeben.
     *
     * @return Liste aller bisherigen Zuege des Spielers
     *
     * @throws Exception
     *         Falls es Netzwerkprobleme gibt oder das Anfordern aller Zuege fehlschlaegt (z. B. beim Aufruf auf
     *         Feenspielern)
     */
    List<Move> requestAllMoves() throws Exception;

    /**
     * Erfragt vom Spieler alle seine Salts die verwendet wurden, um seine bisher ausgefuehrten Zuege zu hashen. Nur der
     * Goblin hasht seine Zuege und deshalb wird diese Methode auch nur beim Goblin nach Spielende aufgerufen. Jeder Zug
     * des Goblins verwendet einen anderen, zufaelligen Salt zum Hashen. Die Feen benoetigen daher auch alle Salts in
     * korrekter Reihenfolge, um die uebermittelten Hashwerte des Goblins verifizieren zu koennen.
     *
     * @return Liste aller bisher verwendeten Salts
     *
     * @throws Exception
     *         Falls es Netzwerkprobleme gibt oder das Anfordern aller Salts fehlschlaegt (z. B. beim Aufruf auf
     *         Feenspielern)
     */
    List<String> requestAllSalts() throws Exception;

    /**
     * Verifiziert die Zuege des Goblins nach Spielende. Diese Methode wird nur bei Feen-Spielern aufgerufen, da der
     * Goblin sich selbst vertrauen kann. Die Feen muessen mithilfe der Liste aller offenen Positionen des Goblins und
     * den Salts seiner Zuege die Hashwerte selbst nachrechnen und verifizieren, dass diese tatsaechlich den Hashwerte
     * entsprechen, die im Laufe des Spiels vom Goblin uebermittelt worden sind. Weiterhin muss geprueft werden, ob der
     * Spielstatus zum Ende des Spiels dem uebergebenen Status entspricht, auf den sich das Hauptspiel geeinigt hat.
     * Sollte dies nicht der Fall sein, muss darauf mit einer Exception reagiert werden.
     *
     * @param status
     *         Der Status zum Spielende, den das Hauptspiel entschieden hat
     * @param moves
     *         Die Liste aller offenen Zuege des Goblins
     * @param salts
     *         Die Liste aller vom Goblin verwendeten Salts
     *
     * @throws Exception
     *         Falls es Netzwerkprobleme gibt oder das Verifizieren fehlschlaegt (der Spieler stimmt dem Spielstatus
     *         nicht zu)
     */
    void verifyGame(GameStatus status, List<Move> moves, List<String> salts) throws Exception;
}
