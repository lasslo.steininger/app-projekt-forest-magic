package forestmagicpp.preset;

import java.io.Serializable;

public class Position implements Serializable, Comparable<Position> {
    public static final Position UNKNOWN = new Position();

    public static final char DELIMITER = '/';
    public static final int MAX_COLUMN = 160;
    public static final int MAX_ROW = 90;
    private static final long serialVersionUID = 1L;

    // ------------------------------------------------------------

    private int column;
    private int row;

    // ------------------------------------------------------------

    private Position() {
        this.column = 0;
        this.row = 0;
    }

    public Position(final int column, final int row) {
        setColumn(column);
        setRow(row);
    }

    // ------------------------------------------------------------

    public static Position parsePosition(final String string) {
        if (string == null || string.equals("")) {
            throw new PositionFormatException("cannot parse empty string");
        }

        if (!string.startsWith("(") || !string.endsWith(")")) {
            throw new PositionFormatException("wrong outer format! correct format is: (COLUMN,ROW)");
        }

        String[] parts = string.substring(1, string.length() - 1)
                               .split(String.valueOf(DELIMITER));

        if (parts.length != 2) {
            throw new PositionFormatException("wrong number of arguments! correct format is: (COLUMN,ROW)");
        }

        try {
            return new Position(Integer.parseInt(parts[0]), Integer.parseInt(parts[1]));
        } catch (NumberFormatException e) {
            throw new PositionFormatException(
                    "wrong number format! valid numbers are 1 to (" + MAX_COLUMN + "/" + MAX_ROW + ")", e);
        }
    }

    public int getColumn() {
        return column;
    }

    private void setColumn(final int column) {
        if (column <= 0 || column > MAX_COLUMN) {
            throw new IllegalArgumentException("illegal column value: " + column);
        }
        this.column = column;
    }

    public int getRow() {
        return row;
    }

    private void setRow(final int row) {
        if (row <= 0 || row > MAX_ROW) {
            throw new IllegalArgumentException("illegal row value: " + row);
        }
        this.row = row;
    }

    // ------------------------------------------------------------

    @Override
    public int hashCode() {
        return getColumn() * MAX_COLUMN + getRow();
    }

    @Override
    public boolean equals(final Object o) {
        if (o == null)
            return false;
        if (!(o instanceof Position))
            return false;
        Position p = (Position) o;
        return getColumn() == p.getColumn() && getRow() == p.getRow();
    }

    @Override
    public String toString() {
        return "(" + getColumn() + DELIMITER + getRow() + ")";
    }

    @Override
    public int compareTo(final Position p) {
        if (getRow() != p.getRow())
            return getRow() - p.getRow();
        return getColumn() - p.getColumn();
    }
}
